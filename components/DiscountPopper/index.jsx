import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Popover, Button, InputNumber, Radio } from 'antd';
import numeral from 'numeral';

const options = [ {value: 'VND', label: 'VND'}, {value: 'percent', label: '%'} ]

const DiscountPopper = (props) => {
   const [option, setOption] = useState('VND')
   const [valueInput, setValueInput] = useState(0);
    const {value = 0, total = 0} = props;
    console.log("🚀 ~ file: index.jsx ~ line 11 ~ DiscountPopper ~ total", total)

    useEffect(() => {
        switch (option) {
            case 'VND':
                setValueInput(value);
                break;
        
            default:
                const result = valueInput * total / 100;
                props.callback(result) 
                break;
        }
    }, [total]);

    const onChangeRadio = (event) => {
        setOption(event.target.value)

    }

    useEffect(() => {
        switch (option) {
            case 'VND':
                setValueInput(value)
                // props.callback(value)
                break;
        
            default:
                let result = total;
                if(valueInput <= 100) {
                    result = valueInput * total / 100;
                }
                props.callback(result) 
                break;
        }
    }, [option])

    const onChangeInput = (value) => {
        if(value > 0) {
            switch (option) {
                case 'VND':
                    props.callback(value)
                    break;
            
                default:
                    const result = value * total / 100;
                    props.callback(result) 
                    break;
            }
    
            setValueInput(value)
        }
    }

    const content = (
        <div style={{display: 'flex', alignItems: 'center', gap: '5px'}}>
          <strong>Giảm giá</strong>
          <InputNumber 
            onChange={onChangeInput} 
            value={valueInput} 
            formatter={value => option === 'VND' ? `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',') : value}
            parser={value => option === 'VND' ? value.replace(/(,*)/g, '') : value} 
            min={0} 
            max={option === 'VND' ? undefined : 100} 
            size='small' 
            style={{width: 120}}
            />
          <Radio.Group
            size='small'
            options={options}
            onChange={onChangeRadio}
            value={option}
            optionType="button"
            />
        </div>
      );

    return (
        <Popover placement='left' content={content} trigger="click">
        <Button size='small' >{numeral(value).format('0,0')} đ</Button>
      </Popover>
    );
};

DiscountPopper.propTypes = {};

export default DiscountPopper;