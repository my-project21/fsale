// Libaries
import React, { useEffect, useMemo, useRef, useState } from 'react';
import {
    Layout,
    Menu,
    Input,
    Dropdown,
    Row,
    Col,
    Tooltip,
    Tabs,
    Empty,
    Button,
    Select,
    Divider,
    InputNumber,
    Popover,
    Modal
} from 'antd';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import numeral from 'numeral';
import moment from 'moment';
import { useDispatch, useSelector } from 'react-redux';
import { useRouter } from 'next/router'
import Link from 'next/link'


// Redux toolkit
import {
    getOrderPayment,
    selectOrderPayment
} from './saleManagerSlice'

// Components
import DiscountPopper from '../DiscountPopper/index'

// Icons
import { SearchOutlined, AudioOutlined, BarcodeOutlined, MenuOutlined, DeleteOutlined, EditOutlined } from '@ant-design/icons'

// Utils
import { random } from '../../utils';

// Styles
import styles from './styles.module.scss';
import { getRenderPropValue } from 'antd/lib/_util/getRenderPropValue';

const { Header, Content, Footer } = Layout;
const { TabPane } = Tabs;
const { Option } = Select;

export function getServerSideProps(context) {
    return {
        props: { params: context.params }
    };
}

// Variable
const initialCategory = [
    {
        key: 'shoes', icon: 'icon-fsale-sneakers', label: 'Shoes', products: [
            { code: 'S001', label: 'Ultraboost 20 Shoes', img: 'images/ultraboost_20.jpg', price: 5000000, amount: 20 },
            { code: 'S002', label: 'STAR WARS NMD_R1 V2 SHOES', img: 'images/star_wars_nmd_r1.jpg', price: 4500000, amount: 24 },
            { code: 'S003', label: 'ZX 8000 LEGO SHOES', img: 'images/ZX_8000_LEGO_Shoes_Yellow_FZ3482_01_standard.jpg', price: 3100000, amount: 0 },
            { code: 'S004', label: 'STAN SMITH SHOES', img: 'images/Stan_Smith_Shoes_White_M20324_01_standard.jpg', price: 2300000, amount: 1 },
        ]
    },
    {
        key: 'cloths', icon: 'icon-fsale-sneakers', label: 'Cloths', products: [
            { code: 'C001', label: 'HUMAN MADE SHORT SLEEVE TEE', img: 'images/Human_Made_Short_Sleeve_Tee_White_GM4255_01_laydown.jpg', price: 5000000, amount: 12 },
            { code: 'C002', label: 'HUMAN MADE SWEATSHIRT', img: 'images/Human_Made_Sweatshirt_Blue_GM4268_01_laydown.jpg', price: 3000000, amount: 2 },
            { code: 'C003', label: 'UB COLORBLOCK JACKET', img: 'images/UB_Colorblock_Jacket_Blue_GL0401_01_laydown.jpg', price: 2300000, amount: 90 },
            { code: 'C004', label: 'OWN THE RUN TWO-IN-ONE SHORTS', img: 'images/Own_the_Run_Two_in_One_Shorts_Blue_GC7882_01_laydown.jpg', price: 700000, amount: 90 }
        ]
    }
]

const staffs = [
    { id: 'staff-1', name: 'Nguyễn Lương Trường Vĩ' },
    { id: 'staff-2', name: 'Nguyễn Phi Hùng' },
    { id: 'staff-3', name: 'Trần Bá Nha' },
    { id: 'staff-4', name: 'Phùng Thị Nhi' },
]

const customers = [
    { id: 'customer-1', name: 'Nguyễn Thị Thập', phone: '0982787652' },
    { id: 'customer-2', name: 'Trần Bá Phước', phone: '0989747652' },
    { id: 'customer-3', name: 'Lưu Thị Lý', phone: '0989787612' },
    { id: 'customer-4', name: 'Ưng Hoàng Kha', phone: '0989797652' },
]

const getProduct = (code) => {
    let newProduct = {};
    initialCategory.map(category => {
        const match = category.products.find(product => product.code === code);

        if (match) {
            newProduct = match
        }
    })

    return newProduct || {};
}

const initialOrders = [
    {
        key: random(5),
        title: 'Đơn 1',
        orderNumber: 1,
        closable: false,
        infoOrder: {
            staff: 'staff-1',
            customer: 'customer-1',
            amount: 0,
            total: 0,
            discount: 0,
            customer_pay: 0,
            customer_give: 0,
            change: 0,
            note: ''
        },
        products: []
    }
]

const actions = [
    { key: 'add_services', label: 'Thêm dịch vụ', icon: 'icon-fsale-box-add' },
    { key: 'discount', label: 'Chiết khấu đơn', icon: 'icon-fsale-price-tag' },
    { key: 'promotion', label: 'Khuyến mãi', icon: 'icon-fsale-gift' },
    { key: 'note_order', label: 'Ghi chú đơn hàng', icon: 'icon-fsale-file-text' },
    { key: 'customer_info', label: 'Thông tin khách hàng', icon: 'icon-fsale-user' },
    { key: 'list_order', label: 'Danh sách đơn hàng', icon: 'icon-fsale-cart' },
    { key: 'report', label: 'Xem báo cáo', icon: 'icon-fsale-clipboard' },
    { key: 'delete_all', label: 'Xóa toàn bộ sản phẩm', icon: 'icon-fsale-bin', danger: true },
]

const initialInfoOrder = [
    { key: 'amount', label: 'Số lượng sản phẩm', value: 0 },
    { key: 'total', label: 'Tổng tiền', value: 0 },
    { key: 'discount', label: 'Giảm giá', value: 0 },
    { key: 'customer_pay', label: 'Khách phải trả', value: 0 },
    { key: 'customer_give', label: 'Tiền khách đưa', value: 0 },
    { key: 'change', label: 'Tiền thừa', value: 0 },
    { key: 'note', label: 'Ghi chú', value: '' }
]

const menu = (
    <Menu style={{ width: 300, fontSize: 16 }}>
        <Menu.Item key="0" className={styles['menu-header__item']}>
            <div>
                <Link href='/SaleOrders'>
                    <div>
                        <i className='icon-fsale-menu'></i>
                        <strong>Danh sách đơn hàng</strong>
                    </div>
                </Link>
            </div>
        </Menu.Item>
        <Menu.Item key="1" className={styles['menu-header__item']}>
            <i className='icon-fsale-stats-dots'></i>
            <strong>Thống kê</strong>
        </Menu.Item>
    </Menu>
);

const SaleManager = (props) => {
    const router = useRouter()

    // State
    const [listProduct, setListProduct] = useState([])
    const [listOrder, setListOrder] = useState([...initialOrders])
    const [orderSelected, setOrderSelected] = useState({ ...initialOrders[0] })
    const [updateInfoOrder, setUpdateInfoOrder] = useState(false);
    const [valueSearch, setValueSearch] = useState('');
    const time = moment().locale('vi').format('DD/MM/YYYY HH:mm')
    const { query = {} } = router

    const { customerId = '' } = query;

    // Redux toolkit
    const dispatch = useDispatch();

    const idxCurrentOrder = useMemo(() => {
        return listOrder.findIndex(order => order.key === orderSelected.key)
    }, [orderSelected]);

    useEffect(() => {
        let newListProduct = [];

        initialCategory.forEach(category => {
            newListProduct = newListProduct.concat(category.products)
        })

        setListProduct(newListProduct)
    }, [])

    useEffect(() => {
        const nListOrder = [...listOrder];
        const indexMatch = listOrder.findIndex(order => order.key === orderSelected.key);
        let nOrder = nListOrder.find(order => order.key === orderSelected.key);

        const { products = [] } = nOrder;
        const total = products.reduce((accumulator, currentValue) => accumulator + (currentValue.total), 0)
        const amount = products.reduce((accumulator, currentValue) => accumulator + (currentValue.amount), 0)

        nOrder = {
            ...nOrder,
            infoOrder: {
                ...nOrder.infoOrder,
                amount: amount,
                customer: customerId ? customerId : 'customer-1',
                total: total,
                customer_pay: total > 0 ? total - nOrder.infoOrder.discount : 0,
                discount: total > 0 ? nOrder.infoOrder.discount : 0,
                change: total > 0 ? nOrder.infoOrder.customer_give - (total - nOrder.infoOrder.discount) : 0
            }
        }

        nListOrder[indexMatch] = nOrder;

        setListOrder(nListOrder)

    }, [updateInfoOrder, customerId])

    const tabActions = {
        add: () => {
            let newListOrder = [...listOrder];
            let lastOrder = newListOrder[newListOrder.length - 1];

            newListOrder.push({
                key: random(5),
                title: `Đơn ${lastOrder.orderNumber + 1}`,
                orderNumber: lastOrder.orderNumber + 1,
                closable: true,
                infoOrder: {
                    staff: 'staff-1',
                    customer: 'customer-1',
                    amount: 0,
                    total: 0,
                    discount: 0,
                    customer_pay: 0,
                    customer_give: 0,
                    change: 0,
                    note: ''
                },
                products: []
            })

            newListOrder[0].closable = true;

            setListOrder(newListOrder)

            setOrderSelected(newListOrder[newListOrder.length - 1])
        },
        remove: (targetKey) => {
            let orderIndex = listOrder.findIndex(order => order.key === targetKey);
            let newListOrder = [...listOrder];

            if (orderIndex !== -1 && listOrder.length !== 1) {

                newListOrder.splice(orderIndex, 1);

                setListOrder(newListOrder);
            }

            if (listOrder.length === 2) {
                newListOrder[0].closable = false;

                setListOrder(newListOrder)
            }

            setOrderSelected(newListOrder[0])
        }
    }

    const renderListProduct = (listProduct) => {
        const indexOrder = listOrder.findIndex(order => order.key === orderSelected.key)

        if (Array.isArray(listProduct)) {
            return (
                <Menu className={styles['menu-list-product']}>
                    {
                        listProduct.map((product) => {
                            if (product.label.toLowerCase().includes(valueSearch.toLowerCase()) || product.code.toLowerCase().includes(valueSearch.toLowerCase())) {
                                let isOutOfStock = product.amount === 0;

                                return (
                                    <Menu.Item onClick={() => onSelectProductStock(product, indexOrder)} disabled={isOutOfStock} style={{ display: 'flex', alignItems: 'center', opacity: isOutOfStock ? 0.6 : 1 }} key={product.code}>
                                        <img src={product.img} width={60} />
                                        <div className={styles['content-product']}>
                                            <div className={styles['d-flex-between']}>
                                                <Tooltip title={product.label}>
                                                    <div className={`${styles['title']} title-product-a1`}>{product.label}</div>
                                                </Tooltip>
                                                <span>{numeral(product.price).format('0,0')} vnđ</span>
                                            </div>
                                            <div className={styles['d-flex-between']}>
                                                <div>{product.code}</div>
                                                <span style={{ color: isOutOfStock ? '#0050b3' : 'unset' }}>Số  lượng: {product.amount}</span>
                                            </div>
                                        </div>
                                    </Menu.Item>
                                )
                            }
                        })
                    }
                </Menu>
            )
        }
    }

    const renderMenuProducts = (menu) => {
        if (Array.isArray(menu)) {
            return (
                <Menu defaultOpenKeys={[menu[0].key]} triggerSubMenuAction='click' >
                    {
                        menu.map(category => (
                            <Menu.SubMenu key={category.key} icon={<i className={`${category.icon} ${styles['icon-category']}`} />} title={<strong>{category.label}</strong>}>
                                {category.products && category.products.map(product => (
                                    <Menu.Item style={{ display: 'flex', alignItems: 'center' }} key={product.code}>
                                        <img src={product.img} width={50} />
                                        <div className={styles['content-product']}>
                                            <div className={styles['d-flex-between']}>
                                                <div className={styles['title']}>{product.label}</div>
                                                <span>{numeral(product.price).format('0,0')} vnđ</span>
                                            </div>
                                            <div className={styles['d-flex-between']}>
                                                <div>{product.code}</div>
                                                <span>{moment().format('DD/MM/YYYY')}</span>
                                            </div>
                                        </div>
                                    </Menu.Item>
                                ))}
                            </Menu.SubMenu>
                        ))
                    }
                </Menu>
            )
        }

        return [];
    }

    const onChangeTabs = (orderKey) => {
        let newOrder = listOrder.find(order => order.key === orderKey);

        if (newOrder) {
            setOrderSelected(newOrder)
        }
    }

    const onEditTabs = (targetKey, action) => {
        tabActions[action](targetKey)
    }

    const onChangeAmount = (value, index, indexOrder) => {
        const newOrders = [...listOrder];

        newOrders[indexOrder].products[index].amount = value;

        const { price, amount } = newOrders[indexOrder].products[index]

        const total = price * amount;

        newOrders[indexOrder].products[index].total = total;

        setListOrder(newOrders)

        setUpdateInfoOrder(!updateInfoOrder)

    }

    const onChangePrice = (value, index, indexOrder) => {
        const newOrders = [...listOrder];

        newOrders[indexOrder].products[index].price = value;

        const { price, amount } = newOrders[indexOrder].products[index]

        const total = price * amount;

        newOrders[indexOrder].products[index].total = total;

        setListOrder(newOrders)

        setUpdateInfoOrder(!updateInfoOrder)
    }

    const onChangeTotal = (value, index, indexOrder) => {
        const newOrders = [...listOrder];

        newOrders[indexOrder].products[index].total = value;

        setListOrder(newOrders)
    }

    const onClickDelete = (index, indexOrder) => {
        const newOrders = [...listOrder];

        newOrders[indexOrder].products.splice(index, 1)

        setListOrder(newOrders)

        setUpdateInfoOrder(!updateInfoOrder)
    }

    const onChangeNote = (event, index, indexOrder) => {
        const newOrders = [...listOrder];
        const { value } = event.target;

        newOrders[indexOrder].products[index].note = value;

        setListOrder(newOrders)
    }

    const renderContent = (products, indexOrder) => {

        return (
            <div className={styles['content-order']}>
                {products && products.length ? products.map((product, index) => {
                    const infoProduct = getProduct(product.code)

                    return (
                        <div key={product.code} className={styles['wrap-product-buy']}>
                            <div className='d-flex-center' style={{ gap: 20 }}>
                                <strong>{index + 1}</strong>
                                <DeleteOutlined style={{ fontSize: 15 }} onClick={() => onClickDelete(index, indexOrder)} className={styles['icon-delete']} />
                                <div style={{ maxWidth: 200 }}>
                                    <strong style={{ fontSize: 14 }}>{infoProduct.label}</strong>
                                    <Input className='input-note' value={product.note} onChange={(event) => onChangeNote(event, index, indexOrder)} style={{ color: '#595959' }} prefix={<EditOutlined />} placeholder="Ghi chú" bordered={false} />
                                </div>
                            </div>
                            <div className='d-flex-center' style={{ gap: 5 }}>
                                <div>
                                    <strong>Số lượng</strong> <br />
                                    <InputNumber min={1} value={product.amount} onChange={(value) => onChangeAmount(value, index, indexOrder)} />
                                </div>
                            </div>
                            <div className='d-flex-center' style={{ gap: 5 }}>
                                <div>
                                    <strong>Giá</strong> <br />
                                    <InputNumber
                                        formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                        parser={value => value.replace(/(,*)/g, '')}
                                        min={0} style={{ width: 150 }}
                                        value={product.price} onChange={(value) => onChangePrice(value, index, indexOrder)} />
                                </div>
                            </div>
                            <div className='d-flex-center' style={{ gap: 5 }}>
                                <div style={{ textAlign: 'end' }}>
                                    <strong>Tổng cộng</strong> <br />
                                    <div style={{ fontWeight: 'bold', color: '#237804', fontSize: 16 }}>{numeral(product.total).format('0,0')} đ</div>
                                </div>
                            </div>
                        </div>
                    )
                }) : (
                        <Empty
                            image={<i className='icon-fsale-box' style={{ fontSize: 100, color: '#1890ff' }} />}
                            className='d-flex-center column'
                            description={false}
                        >
                            <div className='d-flex-center column' style={{ gap: '10px 0px' }}>
                                <span style={{ fontSize: 18, color: '#1890ff' }}>Đơn hàng của bạn chưa có sản phẩm nào</span>
                            </div>
                        </Empty>
                    )}
            </div >
        )
    }

    const callbackDiscount = (value) => {
        const nListOrder = [...listOrder];
        const idx = nListOrder.findIndex(order => order.key === orderSelected.key)
        const nOrder = [...listOrder].find(order => order.key === orderSelected.key);

        nOrder.infoOrder.discount = value;

        nListOrder[idx] = nOrder;

        setListOrder(nListOrder)

        setUpdateInfoOrder(!updateInfoOrder)
    }

    const onChangeCustomGive = (value) => {
        const nListOrder = [...listOrder];
        const idx = nListOrder.findIndex(order => order.key === orderSelected.key)

        nListOrder[idx].infoOrder.customer_give = value;

        setListOrder(nListOrder)

        setUpdateInfoOrder(!updateInfoOrder)
    }

    const showRenderField = (field, value, total) => {
        switch (field.key) {
            case 'total':
                return (
                    <div className={styles['d-flex-space']}>
                        <span>{field.label}</span>
                        <span>{numeral(value).format('0,0')} đ</span>
                    </div>

                )
            case 'amount':
                return (
                    <div className={styles['d-flex-space']}>
                        <span>{field.label}</span>
                        <span>{value}</span>
                    </div>

                )
            case 'note':
                return (
                    <div className='input-note '>
                        <Input style={{ color: '#595959', padding: '10px 0px' }} prefix={<EditOutlined />} bordered={false} placeholder='Ghi chú' />
                    </div>
                )
            case 'discount':
                return (
                    <div className={styles['d-flex-space']}>
                        <span>{field.label}</span>
                        <DiscountPopper value={value} total={total} callback={callbackDiscount} />
                    </div>
                )
            case 'customer_pay':
                return (
                    <div className={styles['d-flex-space']}>
                        <span>{field.label}</span>
                        <strong style={{ color: '#237804' }}>{numeral(value).format('0,0')} đ</strong>
                    </div>
                )
            case 'change':
                return (
                    <div className={styles['d-flex-space']}>
                        <span>{field.label}</span>
                        <strong>{numeral(value).format('0,0')} đ</strong>
                    </div>
                )
            default:
                return (
                    <div className={styles['d-flex-space']}>
                        <span>{field.label}</span>
                        <InputNumber formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')} parser={value => value.replace(/(,*)/g, '')} value={value} size='small' onChange={onChangeCustomGive} />
                    </div>
                )
        }
    }

    const onChangeStaff = (value) => {
        const nListOrder = [...listOrder]
        const idx = listOrder.findIndex(order => order.key === orderSelected.key);

        nListOrder[idx].infoOrder.staff = value

        setListOrder(nListOrder)
    }

    const onChangeCustomer = (value) => {
        const nListOrder = [...listOrder]
        const idx = listOrder.findIndex(order => order.key === orderSelected.key);

        nListOrder[idx].infoOrder.customer = value

        setListOrder(nListOrder)
    }

    const onClickPayment = () => {
        const dOrder = listOrder.find(order => order.key === orderSelected.key);
        const { customer_pay } = dOrder.infoOrder;

        // Info customer, staff for show detail invoice
        const nCustomer = customers.find(customer => customer.id === dOrder.infoOrder.customer);
        const nStaff = staffs.find(staff => staff.id == dOrder.infoOrder.staff);


        Modal.confirm({
            title: 'Xác nhận thanh toán',
            icon: <ExclamationCircleOutlined style={{ color: '#1890ff' }} />,
            content: `Bạn có muốn xác nhận thanh toán với đơn hàng ${numeral(customer_pay).format('0,0')} đ`,
            onOk() {
                dispatch(getOrderPayment({
                    orderPayment: {
                        ...dOrder,
                        infoOrder: {
                            ...dOrder.infoOrder,
                            staff: nStaff,
                            customer: nCustomer,
                            time: time
                        }
                    }
                }))

                router.push('/InVoice')
            },
            onCancel() {
                console.log('Cancel');
            },
            okText: "Đồng ý",
            cancelText: "Hủy bỏ"
        });
    }

    const renderInfoOrder = (nInfoOrder) => {
        const { staff, customer, total } = nInfoOrder;

        const filterStaff = staffs.find(nStaff => nStaff.id === staff);
        const filterCustomer = customers.find(nCustomer => nCustomer.id === customer);

        return (
            < >
                <div>
                    <div className={styles['info__row']}>
                        <strong>Nhân viên bán hàng</strong>
                        <strong>Ngày bán</strong>
                    </div>
                    <Row>
                        <Col xs={{ span: 24 }} md={{ span: 12 }}>
                            <Select
                                showSearch
                                style={{ width: '100%' }}
                                placeholder="Select a person"
                                optionFilterProp="children"
                                defaultValue={staffs[0].id}
                                value={filterStaff.id}
                                onChange={(value) => onChangeStaff(value)}
                                filterOption={(input, option) =>
                                    option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                }
                            >
                                {staffs.map(staff => (
                                    <Option key={staff.id} value={staff.id} >{staff.name}</Option>
                                ))}
                            </Select>
                        </Col>
                        <Col xs={{ span: 24 }} md={{ span: 12 }} style={{ textAlign: 'end' }}>
                            {time}
                        </Col>
                    </Row>
                    <div className={styles['info__row']}>
                        <strong>Khách hàng</strong>
                        <strong>Số điện thoại</strong>
                    </div>
                    <Row>
                        <Col xs={{ span: 24 }} md={{ span: 12 }}>
                            <Select
                                showSearch
                                style={{ width: '100%' }}
                                placeholder="Chọn khách hàng"
                                optionFilterProp="children"
                                defaultValue={customers[0].id}
                                value={filterCustomer.id}
                                onChange={(value) => onChangeCustomer(value)}
                                filterOption={(input, option) =>
                                    option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                }
                            >
                                {customers.map(customer => (
                                    <Option key={customer.id} value={customer.id} >{customer.name}</Option>
                                ))}
                            </Select>
                        </Col>
                        <Col xs={{ span: 24 }} md={{ span: 12 }} style={{ textAlign: 'end' }}>
                            {filterCustomer.phone}
                        </Col>
                    </Row>
                    <Divider style={{ margin: '10px 0px 0px 0px', borderTop: '1px solid #bfbfbf' }} />
                    {Object.keys(nInfoOrder).map((field, index) => {
                        const formatOrder = initialInfoOrder.find(order => order.key === field);
                        const value = nInfoOrder[field]

                        if (formatOrder) {
                            return (
                                <React.Fragment key={formatOrder.key} >
                                    {showRenderField(formatOrder, value, total)}
                                </React.Fragment>
                            )
                        }

                    })}
                </div>
                <Button
                    disabled={listOrder[idxCurrentOrder].infoOrder.total === 0 ? true : false}
                    style={{
                        color: '#fff',
                        fontWeight: 500,
                        fontSize: 30,
                        height: 60,
                        backgroundColor: '#34D789',
                        width: '100%',
                        opacity: listOrder[idxCurrentOrder].infoOrder.total === 0 ? 0.7 : 1
                    }}
                    size='middle'
                    onClick={onClickPayment}
                >
                    Thanh toán
                </Button>
            </>
        )
    }

    const onSelectProductStock = (product, indexOrder) => {
        const saleProduct = {
            code: product.code,
            amount: 1,
            total: product.price,
            price: product.price,
            note: ''
        }

        const newOrders = [...listOrder]

        const indexMatch = newOrders[indexOrder].products.findIndex(nProduct => nProduct.code === product.code);

        if (indexMatch !== -1) {
            const { total, price, amount } = newOrders[indexOrder].products[indexMatch];

            newOrders[indexOrder].products[indexMatch].amount += 1;
            newOrders[indexOrder].products[indexMatch].total = (amount + 1) * price;
        } else {
            newOrders[indexOrder].products.push(saleProduct);
        }

        setListOrder(newOrders)

        setUpdateInfoOrder(!updateInfoOrder)
    }

    const renderStockProducts = (indexOrder) => {
        if (Array.isArray(listProduct)) {
            return (
                <>
                    {
                        listProduct.map((product) => {
                            return (
                                <Col onClick={() => onSelectProductStock(product, indexOrder)} key={product.code} xs={{ span: 8 }} md={{ span: 6 }} className={styles['product-item__wrapper']}>
                                    <img src={product.img} alt="" width={100} height={100} />
                                    <span className={styles['stock-product-money__label']}>
                                        {numeral(product.price).format('0,0')}
                                    </span>
                                    <strong>{product.label}</strong>
                                </Col>
                            )
                        })
                    }
                </>
            )
        }
    }

    const onClickAudio = () => {
        setValueSearch('shoes')
    }

    return (
        <Layout>
            <Content className={styles['main-content']}>
                <Tabs
                    className={styles['main-content__tabs']}
                    tabBarStyle={{ background: '#36cfc9' }}
                    activeKey={orderSelected.key}
                    type='editable-card'
                    onChange={onChangeTabs}
                    onEdit={onEditTabs}
                    tabBarExtraContent={{
                        left: (
                            <div className={styles['sale-manager-header']}>
                                <Dropdown trigger={['click']} overlay={renderListProduct(listProduct)}>
                                    <div>
                                        <Input value={valueSearch} onChange={(event) => { setValueSearch(event.target.value) }} prefix={<SearchOutlined style={{ color: '#006d75' }} />} suffix={<AudioOutlined onClick={onClickAudio} style={{ color: '#006d75', cursor: 'pointer' }} />} size='large' className={styles['input-search']} placeholder="Nhập để tìm kiếm sản phẩm" />
                                    </div>
                                </Dropdown>
                                <BarcodeOutlined style={{ color: '#fff', fontSize: 25, cursor: 'poin ter' }} onClick={() => onSelectProductStock(initialCategory[0].products[Math.floor(Math.random() * 4)], idxCurrentOrder)} />
                            </div>
                        ),
                        right: (
                            <Dropdown overlay={menu} trigger={['click']}>
                                <MenuOutlined style={{ color: '#fff', fontSize: 25, margin: '0px 10px' }} />
                            </Dropdown>

                        )
                    }}
                >
                    {Array.isArray(listOrder) && listOrder.map((order, index) => (
                        <TabPane
                            tab={order.title}
                            key={order.key}
                            closable={order.closable}
                        >
                            <Row style={{ width: '100%', position: 'relative', height: '100%', padding: 10 }} gutter={20}>
                                <Col xs={{ span: 24 }} md={{ span: 18 }} className={styles['left-content']}>
                                    {renderContent(order.products, index)}
                                    <Row className={styles['main-content-list-item__footer']} >
                                        {renderStockProducts(index)}
                                    </Row>
                                </Col>
                                <Col xs={{ span: 24 }} md={{ span: 6 }} style={{ padding: 10 }} className={styles['right-content']}>
                                    {renderInfoOrder(order.infoOrder)}
                                </Col>
                            </Row>
                        </TabPane>
                    ))}
                </Tabs>
            </Content>
        </Layout>
    );
};

export default SaleManager;