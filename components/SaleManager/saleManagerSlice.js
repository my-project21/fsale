import { createSlice } from '@reduxjs/toolkit';

export const saleManagerSlice = createSlice({
    name: 'saleManager',
    initialState: {
        orderPayment: {},
        initialCategory: [
            {
                key: 'shoes', icon: 'icon-fsale-sneakers', label: 'Shoes', products: [
                    { code: 'S001', label: 'Ultraboost 20 Shoes', img: 'images/ultraboost_20.jpg', price: 5000000, amount: 20 },
                    { code: 'S002', label: 'STAR WARS NMD_R1 V2 SHOES', img: 'images/star_wars_nmd_r1.jpg', price: 4500000, amount: 24 },
                    { code: 'S003', label: 'ZX 8000 LEGO SHOES', img: 'images/ZX_8000_LEGO_Shoes_Yellow_FZ3482_01_standard.jpg', price: 3100000, amount: 0 },
                    { code: 'S004', label: 'STAN SMITH SHOES', img: 'images/Stan_Smith_Shoes_White_M20324_01_standard.jpg', price: 2300000, amount: 1 },
                ]
            },
            {
                key: 'cloths', icon: 'icon-fsale-sneakers', label: 'Cloths', products: [
                    { code: 'C001', label: 'HUMAN MADE SHORT SLEEVE TEE', img: 'images/Human_Made_Short_Sleeve_Tee_White_GM4255_01_laydown.jpg', price: 5000000, amount: 12 },
                    { code: 'C002', label: 'HUMAN MADE SWEATSHIRT', img: 'images/Human_Made_Sweatshirt_Blue_GM4268_01_laydown.jpg', price: 3000000, amount: 2 },
                    { code: 'C003', label: 'UB COLORBLOCK JACKET', img: 'images/UB_Colorblock_Jacket_Blue_GL0401_01_laydown.jpg', price: 2300000, amount: 90 },
                    { code: 'C004', label: 'OWN THE RUN TWO-IN-ONE SHORTS', img: 'images/Own_the_Run_Two_in_One_Shorts_Blue_GC7882_01_laydown.jpg', price: 700000, amount: 90 }
                ]
            }
        ]
    },
    reducers: {
        getOrderPayment: (state, action) => {
            const { orderPayment } = action.payload;

            state.orderPayment = orderPayment;
        },
        increment: state => {
            // Redux Toolkit allows us to write "mutating" logic in reducers. It
            // doesn't actually mutate the state because it uses the Immer library,
            // which detects changes to a "draft state" and produces a brand new
            // immutable state based off those changes
            state.value += 1;
        },
        decrement: state => {
            state.value -= 1;
        },
        incrementByAmount: (state, action) => {
            state.value += action.payload;
        },
    },
});

export const { increment, decrement, incrementByAmount, getOrderPayment } = saleManagerSlice.actions;

// The function below is called a thunk and allows us to perform async logic. It
// can be dispatched like a regular action: `dispatch(incrementAsync(10))`. This
// will call the thunk with the `dispatch` function as the first argument. Async
// code can then be executed and other actions can be dispatched
export const incrementAsync = amount => dispatch => {
    setTimeout(() => {
        dispatch(incrementByAmount(amount));
    }, 1000);
};

// The function below is called a selector and allows us to select a value from
// the state. Selectors can also be defined inline where they're used instead of
// in the slice file. For example: `useSelector((state) => state.counter.value)`
export const selectOrderPayment = state => state.saleManager.orderPayment;
export const selectInitialCategory = state => state.saleManager.initialCategory;

export default saleManagerSlice.reducer;
