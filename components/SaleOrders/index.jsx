// Libraries
import React, { useRef, useState, useMemo } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import PropTypes from 'prop-types';
import { Layout, Typography, Input, Button, Table, Checkbox, Space, DatePicker, Modal, Dropdown, Menu, Tabs, notification } from 'antd';
import numeral from 'numeral';
import Highlighter from 'react-highlight-words';
import Link from 'next/link'
import moment from 'moment'
// Icons
import { HomeOutlined, EllipsisOutlined, MenuOutlined, SwapOutlined, EditOutlined, SearchOutlined, AudioOutlined, DeleteOutlined, ShoppingCartOutlined, FilterOutlined } from '@ant-design/icons';

// Styles
import styles from './styles.module.scss'

// Redux toolkit
import { selectDetailOrder, selectOrders, setDetailOrder } from './saleOrdersSlice'

const { TabPane } = Tabs;
// Antd
const { Header, Content, Footer } = Layout;
const { Title, Text } = Typography;

const constant = {
    paymentType: ['Ngân hàng', 'COD', 'Tiền mặt'],
    status: [{ label: 'Đã giao', color: '#531dab' }, { label: 'Đã thanh toán', color: '#52c41a' }, { label: 'Đang chờ', color: '#d48806' }, { label: 'Đang giao', color: '#13c2c2' }]
}

const customers = [
    { id: 'customer-1', name: 'Nguyễn Thị Thập', phone: '0982787652', email: "thithap@gmail.com", address: '66/9 binh loi' },
    { id: 'customer-2', name: 'Trần Bá Phước', phone: '0989747652', email: "baphuoc@gmail.com", address: '12/3 nguyen loi ca' },
    { id: 'customer-3', name: 'Lưu Thị Lý', phone: '0989787612', email: "hahoang@gmail.com", address: '90 loi binh khiem' },
    { id: 'customer-4', name: 'Ưng Hoàng Kha', phone: '0989797652', email: "khoikha@gmail.com", address: '78 hoang kha hung' },
]

const tabDelivery = [
    { key: 'time', label: 'Cập nhật thời gian giao hàng', titleTab: 'Thời gian giao hàng' },
    { key: 'address', label: 'Cập nhật địa chỉ giao hàng', titleTab: 'Địa chỉ giao hàng' },
]

const SaleOrders = () => {
    const [data, setData] = useState([
        {
            key: '1',
            code: 'DH001',
            postingDate: '01/02/2020',
            address: '66/6 Nguyen hong anh',
            prepayment: 200000000,
            customerName: customers[0].name,
            note: 'Giao hàng cẩn thận, hóa đơn đầy đủ',
            total: 171000000,
            paymentType: constant.paymentType[0],
            discounted: true,
            status: constant.status[0],
            products: [
                { key: 'S001', nameProduct: 'Ultraboost 20 Shoes', description: "Mau vang, size s, phien ban gioi han", unit: 'Cai', price: 5000000, discounted: 10, rawAmount: 100000000, amount: 10, total: 90000000 },
                { key: 'S002', nameProduct: 'STAR WARS NMD_R1 V2 SHOES', description: "Mau den", unit: 'Cai', price: 4500000, discounted: 30, rawAmount: 108000000, amount: 24, total: 75600000 },
                { key: 'S003', nameProduct: 'ZX 8000 LEGO SHOES', description: "Ma vang", unit: 'Cai', price: 3100000, discounted: 0, rawAmount: 3100000, amount: 2, total: 3100000 },
                { key: 'S004', nameProduct: 'STAN SMITH SHOES', description: "size xs", unit: 'Cai', price: 2300000, discounted: 0, rawAmount: 2300000, amount: 2, total: 2300000 },
            ]
        },
        {
            key: '2',
            code: 'DH002',
            postingDate: '02/02/2020',
            prepayment: 200000000,
            address: '66/6 Nguyen hong anh',
            customerName: customers[1].name,
            note: 'Giao hàng cẩn thận, hóa đơn đầy đủ',
            total: 171000000,
            paymentType: constant.paymentType[1],
            discounted: true,
            status: constant.status[1],
            products: [
                { key: 'S001', nameProduct: 'Ultraboost 20 Shoes', description: "Mau vang, size s, phien ban gioi han", unit: 'Cai', price: 5000000, discounted: 10, rawAmount: 100000000, amount: 15, total: 90000000 },
                { key: 'S002', nameProduct: 'STAR WARS NMD_R1 V2 SHOES', description: "Mau den", unit: 'Cai', price: 4500000, discounted: 30, rawAmount: 108000000, amount: 3, total: 75600000 },
                { key: 'S003', nameProduct: 'ZX 8000 LEGO SHOES', description: "Ma vang", unit: 'Cai', price: 3100000, discounted: 0, rawAmount: 3100000, amount: 12, total: 3100000 },
                { key: 'S004', nameProduct: 'STAN SMITH SHOES', description: "size xs", unit: 'Cai', price: 2300000, discounted: 0, rawAmount: 2300000, amount: 11, total: 2300000 },
            ]
        },
        {
            key: '3',
            code: 'DH003',
            postingDate: '03/02/2020',
            prepayment: 200000000,
            address: '66/6 Nguyen hong anh',
            customerName: customers[2].name,
            note: 'Giao hàng cẩn thận, hóa đơn đầy đủ',
            total: 171000000,
            paymentType: constant.paymentType[2],
            discounted: true,
            status: constant.status[3],
            products: [
                { key: 'S001', nameProduct: 'Ultraboost 20 Shoes', description: "Mau vang, size s, phien ban gioi han", unit: 'Cai', price: 5000000, discounted: 20, rawAmount: 100000000, amount: 50, total: 90000000 },
                { key: 'S002', nameProduct: 'STAR WARS NMD_R1 V2 SHOES', description: "Mau den", unit: 'Cai', price: 4500000, discounted: 30, rawAmount: 108000000, amount: 6, total: 75600000 },
                { key: 'S003', nameProduct: 'ZX 8000 LEGO SHOES', description: "Ma vang", unit: 'Cai', price: 3100000, discounted: 0, rawAmount: 3100000, amount: 6, total: 3100000 },
                { key: 'S004', nameProduct: 'STAN SMITH SHOES', description: "size xs", unit: 'Cai', price: 2300000, discounted: 0, rawAmount: 2300000, amount: 1, total: 2300000 },
            ]
        },
        {
            key: '4',
            code: 'DH004',
            postingDate: '04/04/2020',
            address: '66/6 Nguyen hong anh',
            prepayment: 200000000,
            customerName: customers[3].name,
            note: 'Giao hàng cẩn thận, hóa đơn đầy đủ',
            total: 171000000,
            paymentType: constant.paymentType[1],
            discounted: true,
            status: constant.status[2],
            products: [
                { key: 'S001', nameProduct: 'Ultraboost 20 Shoes', description: "Mau vang, size s, phien ban gioi han", unit: 'Cai', price: 5000000, discounted: 10, rawAmount: 100000000, amount: 43, total: 90000000 },
                { key: 'S002', nameProduct: 'STAR WARS NMD_R1 V2 SHOES', description: "Mau den", unit: 'Cai', price: 4500000, discounted: 30, rawAmount: 108000000, amount: 24, total: 75600000 },
                { key: 'S003', nameProduct: 'ZX 8000 LEGO SHOES', description: "Ma vang", unit: 'Cai', price: 3100000, discounted: 0, rawAmount: 3100000, amount: 4, total: 3100000 },
                { key: 'S004', nameProduct: 'STAN SMITH SHOES', description: "size xs", unit: 'Cai', price: 2300000, discounted: 0, rawAmount: 2300000, amount: 4, total: 2300000 },
            ]
        },
        {
            key: '5',
            code: 'DH005',
            postingDate: '05/03/2020',
            address: '66/6 Nguyen hong anh',
            prepayment: 200000000,
            customerName: customers[1].name,
            note: 'Giao hàng cẩn thận, hóa đơn đầy đủ',
            total: 171000000,
            paymentType: constant.paymentType[0],
            discounted: true,
            status: constant.status[0],
            products: [
                { key: 'S001', nameProduct: 'Ultraboost 20 Shoes', description: "Mau vang, size s, phien ban gioi han", unit: 'Cai', price: 5000000, discounted: 10, rawAmount: 100000000, amount: 26, total: 90000000 },
                { key: 'S002', nameProduct: 'STAR WARS NMD_R1 V2 SHOES', description: "Mau den", unit: 'Cai', price: 4500000, discounted: 30, rawAmount: 108000000, amount: 12, total: 75600000 },
                { key: 'S003', nameProduct: 'ZX 8000 LEGO SHOES', description: "Ma vang", unit: 'Cai', price: 3100000, discounted: 0, rawAmount: 3100000, amount: 5, total: 3100000 },
                { key: 'S004', nameProduct: 'STAN SMITH SHOES', description: "size xs", unit: 'Cai', price: 2300000, discounted: 0, rawAmount: 2300000, amount: 5, total: 2300000 },
            ]
        },
        {
            key: '7',
            code: 'DH007',
            postingDate: '09/02/2020',
            address: '66/6 Nguyen hong anh',
            prepayment: 200000000,
            customerName: customers[3].name,
            note: 'Giao hàng cẩn thận, hóa đơn đầy đủ',
            total: 171000000,
            paymentType: constant.paymentType[1],
            discounted: true,
            status: constant.status[1],
            products: [
                { key: 'S001', nameProduct: 'Ultraboost 20 Shoes', description: "Mau vang, size s, phien ban gioi han", unit: 'Cai', price: 5000000, discounted: 20, rawAmount: 100000000, amount: 10, total: 90000000 },
                { key: 'S002', nameProduct: 'STAR WARS NMD_R1 V2 SHOES', description: "Mau den", unit: 'Cai', price: 4500000, discounted: 30, rawAmount: 108000000, amount: 24, total: 75600000 },
                { key: 'S003', nameProduct: 'ZX 8000 LEGO SHOES', description: "Ma vang", unit: 'Cai', price: 3100000, discounted: 0, rawAmount: 3100000, amount: 1, total: 3100000 },
                { key: 'S004', nameProduct: 'STAN SMITH SHOES', description: "size xs", unit: 'Cai', price: 2300000, discounted: 0, rawAmount: 2300000, amount: 1, total: 2300000 },
            ]
        },
        {
            key: '8',
            code: 'DH008',
            postingDate: '10/01/2020',
            address: '66/6 Nguyen hong anh',
            prepayment: 200000000,
            customerName: customers[0].name,
            note: 'Giao hàng cẩn thận, hóa đơn đầy đủ',
            total: 171000000,
            paymentType: constant.paymentType[2],
            discounted: true,
            status: constant.status[0],
            products: [
                { key: 'S001', nameProduct: 'Ultraboost 20 Shoes', description: "Mau vang, size s, phien ban gioi han", unit: 'Cai', price: 5000000, discounted: 40, rawAmount: 100000000, amount: 20, total: 90000000 },
                { key: 'S002', nameProduct: 'STAR WARS NMD_R1 V2 SHOES', description: "Mau den", unit: 'Cai', price: 4500000, discounted: 30, rawAmount: 108000000, amount: 14, total: 75600000 },
                { key: 'S003', nameProduct: 'ZX 8000 LEGO SHOES', description: "Ma vang", unit: 'Cai', price: 3100000, discounted: 0, rawAmount: 3100000, amount: 1, total: 3100000 },
                { key: 'S004', nameProduct: 'STAN SMITH SHOES', description: "size xs", unit: 'Cai', price: 2300000, discounted: 0, rawAmount: 2300000, amount: 1, total: 2300000 },
            ]
        },
        {
            key: '9',
            code: 'DH009',
            postingDate: '16/12/2020',
            prepayment: 200000000,
            address: '66/6 Nguyen hong anh',
            customerName: customers[1].name,
            note: 'Giao hàng cẩn thận, hóa đơn đầy đủ',
            total: 171000000,
            paymentType: constant.paymentType[2],
            discounted: true,
            status: constant.status[3],
            products: [
                { key: 'S001', nameProduct: 'Ultraboost 20 Shoes', description: "Mau vang, size s, phien ban gioi han", unit: 'Cai', price: 5000000, discounted: 5, rawAmount: 100000000, amount: 20, total: 90000000 },
                { key: 'S002', nameProduct: 'STAR WARS NMD_R1 V2 SHOES', description: "Mau den", unit: 'Cai', price: 4500000, discounted: 30, rawAmount: 108000000, amount: 24, total: 75600000 },
                { key: 'S003', nameProduct: 'ZX 8000 LEGO SHOES', description: "Ma vang", unit: 'Cai', price: 3100000, discounted: 10, rawAmount: 3100000, amount: 1, total: 3100000 },
                { key: 'S004', nameProduct: 'STAN SMITH SHOES', description: "size xs", unit: 'Cai', price: 2300000, discounted: 20, rawAmount: 2300000, amount: 1, total: 2300000 },
            ]
        },
        {
            key: '10',
            code: 'DH0010',
            postingDate: '03/06/2020',
            address: '66/6 Nguyen hong anh',
            prepayment: 200000000,
            customerName: customers[2].name,
            note: 'Giao hàng cẩn thận, hóa đơn đầy đủ',
            total: 171000000,
            paymentType: constant.paymentType[1],
            discounted: true,
            status: constant.status[0],
            products: [
                { key: 'S001', nameProduct: 'Ultraboost 20 Shoes', description: "Mau vang, size s, phien ban gioi han", unit: 'Cai', price: 5000000, discounted: 10, rawAmount: 100000000, amount: 20, total: 90000000 },
                { key: 'S002', nameProduct: 'STAR WARS NMD_R1 V2 SHOES', description: "Mau den", unit: 'Cai', price: 4500000, discounted: 30, rawAmount: 108000000, amount: 24, total: 75600000 },
                { key: 'S003', nameProduct: 'ZX 8000 LEGO SHOES', description: "Ma vang", unit: 'Cai', price: 3100000, discounted: 0, rawAmount: 3100000, amount: 1, total: 3100000 },
                { key: 'S004', nameProduct: 'STAN SMITH SHOES', description: "size xs", unit: 'Cai', price: 2300000, discounted: 0, rawAmount: 2300000, amount: 1, total: 2300000 },
            ]
        },
        {
            key: '11',
            code: 'DH0011',
            postingDate: '04/08/2020',
            prepayment: 200000000,
            customerName: customers[3].name,
            address: '66/6 Nguyen hong anh',
            note: 'Giao hàng cẩn thận, hóa đơn đầy đủ',
            total: 171000000,
            paymentType: constant.paymentType[0],
            discounted: true,
            status: constant.status[0],
            products: [
                { key: 'S001', nameProduct: 'Ultraboost 20 Shoes', description: "Mau vang, size s, phien ban gioi han", unit: 'Cai', price: 5000000, discounted: 10, rawAmount: 100000000, amount: 20, total: 90000000 },
                { key: 'S002', nameProduct: 'STAR WARS NMD_R1 V2 SHOES', description: "Mau den", unit: 'Cai', price: 4500000, discounted: 30, rawAmount: 108000000, amount: 24, total: 75600000 },
                { key: 'S003', nameProduct: 'ZX 8000 LEGO SHOES', description: "Ma vang", unit: 'Cai', price: 3100000, discounted: 0, rawAmount: 3100000, amount: 1, total: 3100000 },
                { key: 'S004', nameProduct: 'STAN SMITH SHOES', description: "size xs", unit: 'Cai', price: 2300000, discounted: 0, rawAmount: 2300000, amount: 1, total: 2300000 },
            ]
        },
    ])

    const [modalDelivery, setModalDelivery] = useState({
        isOpen: false,
        type: tabDelivery[0]
    })
    const [state, setState] = useState({
        searchText: '',
        searchedColumn: '',
    })
    const [globalSearchText, setGlobalSearchText] = useState('')
    const [deleteOrder, setDeleteOrder] = useState(-1);
    const searchInput = useRef(null)
    const [orderSelected, setOrderSelected] = useState([])

    let newData = useMemo(() => {
        let draftData = [];

        draftData = data.map(order => {
            const total = order.products.reduce((accumulator, currentValue) => {
                const total = currentValue.price * currentValue.amount;
                const discounted = currentValue.discounted * total / 100;

                const final = total - discounted;

                return accumulator + final;
            }, 0)

            return ({
                ...order,
                total
            })
        })

        return draftData.filter(item => {
            let isMatchCustomerName = item.customerName.toLowerCase().indexOf(globalSearchText.toLowerCase()) !== -1
            let isMatchCode = item.code.toLowerCase().indexOf(globalSearchText.toLowerCase()) !== -1
            let isMatchPostingDate = item.postingDate.toLowerCase().indexOf(globalSearchText.toLowerCase()) !== -1
            let isMatchTotal = item.total.toString().toLowerCase().indexOf(globalSearchText.toLowerCase()) !== -1
            let isMatchPaymentType = item.paymentType.toString().toLowerCase().indexOf(globalSearchText.toLowerCase()) !== -1
            let isMatchStatus = item.status.label.toString().toLowerCase().indexOf(globalSearchText.toLowerCase()) !== -1

            if (isMatchCode || isMatchCustomerName || isMatchPostingDate || isMatchTotal || isMatchPaymentType || isMatchStatus) {
                return true
            }

            return false
        })
    }, [globalSearchText, deleteOrder, data]);


    // Redux 
    const dispatch = useDispatch();
    const [updateDate, setUpdateDate] = useState(moment());
    const [updateAddress, setUpdateAddress] = useState('');

    const getColumnSearchProps = (dataIndex, placeholder) => ({
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
            <div style={{ padding: 8 }}>
                <Input
                    ref={searchInput}
                    placeholder={`${placeholder}`}
                    value={selectedKeys[0]}
                    onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
                    style={{ width: 188, marginBottom: 8, display: 'block' }}
                />
                <Space>
                    <Button
                        type="primary"
                        onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
                        icon={<SearchOutlined />}
                        size="small"
                        style={{ width: 90 }} this
                    >
                        Tìm kiếm
              </Button>
                    <Button onClick={() => handleReset(clearFilters)} size="small" style={{ width: 90 }}>
                        Khôi phục
              </Button>
                </Space>
            </div>
        ),
        filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
        onFilter: (value, record) =>
            record[dataIndex]
                ? record[dataIndex].toString().toLowerCase().includes(value.toLowerCase())
                : '',
        onFilterDropdownVisibleChange: visible => {
            if (visible) {
                setTimeout(() => searchInput.current.select(), 100);
            }
        },
        render: (text, record) =>
            state.searchedColumn === dataIndex ? (
                <Highlighter
                    highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
                    searchWords={[state.searchText]}
                    autoEscape
                    textToHighlight={text ? text.toString() : ''}
                />
            ) : placeholder === 'Mã' ? <Link href={`/SaleOrders/${record.code}`} ><a>{text}</a></Link> : text,
    });


    const handleSearch = (selectedKeys, confirm, dataIndex) => {
        confirm();
        setState({
            searchText: selectedKeys[0],
            searchedColumn: dataIndex,
        })
    };

    const handleReset = clearFilters => {
        clearFilters();
        setState({ ...state, searchText: '' });
    };

    const columns = [
        {
            title: 'Mã',
            dataIndex: 'code',
            key: 'code',
            render: text => <a>{text}</a>,
            ...getColumnSearchProps('code', 'Mã'),
        },
        {
            title: 'Tên khách hàng',
            dataIndex: 'customerName',
            key: 'customerName',
            ...getColumnSearchProps('customerName', 'Tên khách hàng'),
        },
        {
            title: 'Ngày bán',
            dataIndex: 'postingDate',
            key: 'postingDate',
        },
        {
            title: 'Loại thanh toán',
            key: 'paymentType',
            dataIndex: 'paymentType',
            width: 200,
            filters: [
                {
                    text: 'Ngân hàng',
                    value: "Ngân hàng",
                },
                {
                    text: 'COD',
                    value: 'COD',
                },
                {
                    text: 'Tiền mặt',
                    value: 'Tiền mặt',
                },
            ],
            filterMultiple: true,
            onFilter: (value, record) => record.paymentType === value,

        },
        {
            title: 'Giảm giá',
            key: 'discounted',
            width: 150,
            filters: [
                {
                    text: 'Có',
                    value: true,
                },
                {
                    text: 'Không',
                    value: false,
                },
            ],
            filterMultiple: false,
            onFilter: (value, record) => record.discounted === value,
            render: (record) => {
                return record.discounted === true ? <Checkbox checked /> : null
            }
        },
        {
            title: 'Tổng cộng',
            key: 'total',
            dataIndex: 'total',
            render: (money) => {
                return <div>{numeral(money).format('0,0')} đ</div>
            },
            // defaultSortOrder: 'descend',
            sorter: (a, b) => a.total - b.total
        },
        {
            title: 'Trạng thái',
            key: 'status',
            dataIndex: 'status',
            filters: [
                {
                    text: 'Đã giao',
                    value: 'Đã giao',
                },
                {
                    text: 'Đã thanh toán',
                    value: 'Đã thanh toán',
                },
                {
                    text: 'Đang giao',
                    value: 'Đang giao',
                },
                {
                    text: 'Đang chờ',
                    value: 'Đang chờ',
                },
            ],
            filterMultiple: true,
            onFilter: (value, record) => record.status.label === value,
            render: (status) => {
                return <Text style={{ color: status.color, fontWeight: 600 }}>{status.label}</Text>
            }
        },
        {
            title: '',
            key: 'options',
            dataIndex: 'options',
            render: (_, record, index) => {
                return (
                    <Dropdown overlay={() => optionMenu(index, record.status)} trigger={['click']}>
                        <Button shape='circle' icon={<EllipsisOutlined />}></Button>
                    </Dropdown>
                )
            }
        }
    ];

    const onClickEditOrder = (index) => {

    }

    const onClickDeleteOrder = (index) => {
        console.log('index', index)
        setDeleteOrder(index);

        newData = []
    }

    const optionMenu = (index, status = {}) => {
        return (
            <Menu>
                <Menu.Item onClick={() => onClickEditOrder(index)} icon={<EditOutlined />}>Sửa</Menu.Item>
                <Menu.Item onClick={() => onClickDeleteOrder(index)} icon={<DeleteOutlined />}>Xóa</Menu.Item>
                {
                    ['Đang giao'].indexOf(status.label) !== -1 ? (
                        <>
                            <Menu.Item onClick={() => onClickUpdateDelivery(index, 'time')} icon={<i className='icon-fsale-clock' style={{ marginRight: 5 }} />}>Cập nhật thời gian giao hàng</Menu.Item>
                            <Menu.Item onClick={() => onClickUpdateDelivery(index, 'address')} icon={<i className='icon-fsale-location' style={{ marginRight: 5 }} />}>Cập nhật địa chỉ giao hàng</Menu.Item>
                        </>
                    ) : null
                }
            </Menu>
        )
    }

    const rowSelection = {
        onChange: (selectedRowKeys, selectedRows) => {
            setOrderSelected(selectedRowKeys)
        }
    };

    const onClickUpdateDelivery = (index, type) => {
        const typeUpdate = tabDelivery.find(tab => tab.key === type)

        setModalDelivery({
            isOpen: true,
            type: typeUpdate
        })

        const order = newData[index];

        setOrderSelected(order);
    }

    const onChangeUpdateTimeDeleivery = (date) => {
        const newDate = moment(date).format('DD/MM/YYYY')

        setOrderSelected({
            ...orderSelected,
            postingDate: date
        })

        setUpdateDate(date);
    }

    const onchangeAddress = (e) => {
        const { value } = e.target;
        setUpdateAddress(value)
    }

    const showRenderUpdate = (type) => {
        switch (type) {
            case 'time':

                return (
                    <>
                        <div className={styles['group-box']} >
                            <div style={{ display: 'inline-grid' }}>
                                <Text>Thời gian hiện tại</Text>
                                <DatePicker value={moment(orderSelected.postingDate, 'DD/MM/YYYY')} format={'DD-MM-YYYY'} disabled />
                            </div>
                            <SwapOutlined style={{ fontSize: 25 }} />
                            <div style={{ display: 'inline-grid' }}>
                                <Text>Thời gian mới</Text>
                                <DatePicker value={updateDate} defaultValue={moment()} format={'DD-MM-YYYY'} onChange={(date) => onChangeUpdateTimeDeleivery(date)} />
                            </div>
                        </div>
                        <Checkbox>Giao trong giờ hành chính</Checkbox>
                    </>
                )
            case 'address':
                return (
                    <div className={styles['group-box']} >
                        <div style={{ display: 'inline-grid' }}>
                            <Text>Địa chỉ hiện tại</Text>
                            <Input.TextArea disabled value={orderSelected.address}></Input.TextArea>
                        </div>
                        <SwapOutlined style={{ fontSize: 25 }} />
                        <div style={{ display: 'inline-grid' }}>
                            <Text>Địa chỉ mới</Text>
                            <Input.TextArea value={updateAddress} onChange={onchangeAddress} placeholder={'Nhập địa chỉ mới'} ></Input.TextArea>
                        </div>
                    </div>
                )
            default:
                break;
        }
    }

    const onOkUpdate = () => {

        notification.success({
            message: 'Cập nhật thông tin giao hàng',
            description: `${modalDelivery.type.label} đơn hàng ${orderSelected.code} thành công`
        })

        const index = data.find(data => data.code === orderSelected.code);

        const draftData = [...data];
        draftData[index] = orderSelected;

        setData(draftData)

        setModalDelivery({
            ...modalDelivery,
            isOpen: false
        })
    }

    const onChangeTab = (key) => {
        try {
            console.log("🚀 ~ file: index.jsx ~ line 558 ~ onChangeTab ~ key", key)
            const tabSelected = tabDelivery.find(tab => tab.key === key);

            setModalDelivery({
                ...modalDelivery,
                type: tabSelected
            })
        } catch (error) {
            //
        }
    }

    const onClickMicro = () => {
        switch (modalDelivery.type.key) {
            case 'address':
                setUpdateAddress('66/9 binh loi phuong 13 quan binh thanh')
                break;

            default:
                setUpdateDate(moment('09-02-2021', 'DD-MM-YYYY'))
                break;
        }
    }

    return (
        <Layout className={'Sale-orders__wrap'}>
            <Header className={styles['Layout__header']} >
                <Link href='/'><HomeOutlined style={{ fontSize: 25 }} /></Link>
                <MenuOutlined style={{ fontSize: 25 }} />
            </Header>
            <Content className={styles['Layout__content']}>
                <Title level={3}>Danh sách đơn hàng</Title>
                <div className={styles['wrap__options']}>
                    <div className="d-flex gap-5">
                        <Input onChange={(e) => setGlobalSearchText(e.target.value)} prefix={<SearchOutlined />} style={{ width: 200, borderRadius: 20 }} placeholder='Tìm kiếm' />
                        <Link href={'/NewOrder'}>
                            <Button className={styles['options__button']} icon={<ShoppingCartOutlined />}>Thêm mới</Button>
                        </Link>
                    </div>
                </div>
                <Table
                    rowSelection={{
                        type: 'checkbox',
                        ...rowSelection,
                    }}
                    onRow={(record, index) => {
                        return {
                            className: index % 2 === 0 ? styles['row-even'] : null
                        }
                    }}
                    columns={columns}
                    dataSource={newData}
                    size='small' />

            </Content>
            <Modal
                bodyStyle={{ padding: 0 }}
                title={'Cập nhật thông tin giao hàng'}
                visible={modalDelivery.isOpen}
                okText={'Cập nhật'}
                cancelText={'Hủy'}
                onCancel={() => { setModalDelivery({ ...modalDelivery, isOpen: false }); setOrderSelected({}) }}
                onOk={onOkUpdate}
            >
                <Tabs defaultActiveKey="time" centered onChange={onChangeTab} activeKey={modalDelivery.type.key}>
                    {tabDelivery.map(tab => {
                        return (
                            <TabPane key={tab.key} tab={tab.label}>
                                <div style={{ padding: '0px 10px 10px 10px' }}>
                                    <div className="d-flex" style={{ justifyContent: 'flex-end' }}>
                                        <AudioOutlined style={{ fontSize: 25 }} onClick={onClickMicro} />
                                    </div>
                                    {showRenderUpdate(tab.key)}
                                </div>
                            </TabPane>
                        )
                    })}
                </Tabs>
            </Modal>
        </Layout>
    );
};

SaleOrders.propTypes = {};

export default SaleOrders;