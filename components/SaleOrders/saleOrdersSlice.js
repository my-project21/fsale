import { createSlice } from '@reduxjs/toolkit';

const constant = {
    paymentType: ['Ngân hàng', 'COD', 'Tiền mặt'],
    status: [{ label: 'Đã giao', color: '#52c41a' }, { label: 'Đã thanh toán', color: '#52c41a' }, { label: 'Đang chờ', color: '#13c2c2' }, { label: 'Đang giao', color: '#13c2c2' }]
}

const staffs = [
    { id: 'staff-1', name: 'Nguyễn Lương Trường Vĩ' },
    { id: 'staff-2', name: 'Nguyễn Phi Hùng' },
    { id: 'staff-3', name: 'Trần Bá Nha' },
    { id: 'staff-4', name: 'Phùng Thị Nhi' },
]

const customers = [
    { id: 'customer-1', name: 'Nguyễn Thị Thập', phone: '0982787652', email: "thithap@gmail.com", address: '66/9 binh loi' },
    { id: 'customer-2', name: 'Trần Bá Phước', phone: '0989747652', email: "baphuoc@gmail.com", address: '12/3 nguyen loi ca' },
    { id: 'customer-3', name: 'Lưu Thị Lý', phone: '0989787612', email: "hahoang@gmail.com", address: '90 loi binh khiem' },
    { id: 'customer-4', name: 'Ưng Hoàng Kha', phone: '0989797652', email: "khoikha@gmail.com", address: '78 hoang kha hung' },
]

export const saleOrdersSlice = createSlice({
    name: 'saleOrder',
    initialState: {
        detailOrder: {},
        orders: [
            {
                key: '1',
                code: 'DH001',
                postingDate: '01/02/2020',
                prepayment: 200000000,
                customerName: customers[0].name,
                customer: customers[0],
                note: 'Giao hàng cẩn thận, hóa đơn đầy đủ',
                total: 171000000,
                paymentType: constant.paymentType[0],
                discounted: true,
                status: constant.status[0],
                products: [
                    { key: 'S001', nameProduct: 'Ultraboost 20 Shoes', description: "Mau vang, size s, phien ban gioi han", unit: 'Cái', price: 5000000, discounted: 10, rawAmount: 100000000, amount: 10, total: 90000000 },
                    { key: 'S002', nameProduct: 'STAR WARS NMD_R1 V2 SHOES', description: "Mau den", unit: 'Cái', price: 4500000, discounted: 30, rawAmount: 108000000, amount: 24, total: 75600000 },
                    { key: 'S003', nameProduct: 'ZX 8000 LEGO SHOES', description: "Ma vang", unit: 'Cái', price: 3100000, discounted: 0, rawAmount: 3100000, amount: 2, total: 3100000 },
                    { key: 'S004', nameProduct: 'STAN SMITH SHOES', description: "size xs", unit: 'Cái', price: 2300000, discounted: 0, rawAmount: 2300000, amount: 2, total: 2300000 },
                ]
            },
            {
                key: '2',
                code: 'DH002',
                postingDate: '02/02/2020',
                prepayment: 200000000,
                customerName: customers[1].name,
                customer: customers[1],
                note: 'Giao hàng cẩn thận, hóa đơn đầy đủ',
                total: 171000000,
                paymentType: constant.paymentType[1],
                discounted: true,
                status: constant.status[1],
                products: [
                    { key: 'S001', nameProduct: 'Ultraboost 20 Shoes', description: "Mau vang, size s, phien ban gioi han", unit: 'Cái', price: 5000000, discounted: 10, rawAmount: 100000000, amount: 15, total: 90000000 },
                    { key: 'S002', nameProduct: 'STAR WARS NMD_R1 V2 SHOES', description: "Mau den", unit: 'Cái', price: 4500000, discounted: 30, rawAmount: 108000000, amount: 3, total: 75600000 },
                    { key: 'S003', nameProduct: 'ZX 8000 LEGO SHOES', description: "Ma vang", unit: 'Cái', price: 3100000, discounted: 0, rawAmount: 3100000, amount: 12, total: 3100000 },
                    { key: 'S004', nameProduct: 'STAN SMITH SHOES', description: "size xs", unit: 'Cái', price: 2300000, discounted: 0, rawAmount: 2300000, amount: 11, total: 2300000 },
                ]
            },
            {
                key: '3',
                code: 'DH003',
                postingDate: '03/02/2020',
                prepayment: 200000000,
                customerName: customers[2].name,
                customer: customers[2],
                note: 'Giao hàng cẩn thận, hóa đơn đầy đủ',
                total: 171000000,
                paymentType: constant.paymentType[2],
                discounted: true,
                status: constant.status[3],
                products: [
                    { key: 'S001', nameProduct: 'Ultraboost 20 Shoes', description: "Mau vang, size s, phien ban gioi han", unit: 'Cái', price: 5000000, discounted: 20, rawAmount: 100000000, amount: 50, total: 90000000 },
                    { key: 'S002', nameProduct: 'STAR WARS NMD_R1 V2 SHOES', description: "Mau den", unit: 'Cái', price: 4500000, discounted: 30, rawAmount: 108000000, amount: 6, total: 75600000 },
                    { key: 'S003', nameProduct: 'ZX 8000 LEGO SHOES', description: "Ma vang", unit: 'Cái', price: 3100000, discounted: 0, rawAmount: 3100000, amount: 6, total: 3100000 },
                    { key: 'S004', nameProduct: 'STAN SMITH SHOES', description: "size xs", unit: 'Cái', price: 2300000, discounted: 0, rawAmount: 2300000, amount: 1, total: 2300000 },
                ]
            },
            {
                key: '4',
                code: 'DH004',
                postingDate: '04/04/2020',
                prepayment: 200000000,
                customerName: customers[3].name,
                customer: customers[3],
                note: 'Giao hàng cẩn thận, hóa đơn đầy đủ',
                total: 171000000,
                paymentType: constant.paymentType[1],
                discounted: true,
                status: constant.status[2],
                products: [
                    { key: 'S001', nameProduct: 'Ultraboost 20 Shoes', description: "Mau vang, size s, phien ban gioi han", unit: 'Cái', price: 5000000, discounted: 10, rawAmount: 100000000, amount: 43, total: 90000000 },
                    { key: 'S002', nameProduct: 'STAR WARS NMD_R1 V2 SHOES', description: "Mau den", unit: 'Cái', price: 4500000, discounted: 30, rawAmount: 108000000, amount: 24, total: 75600000 },
                    { key: 'S003', nameProduct: 'ZX 8000 LEGO SHOES', description: "Ma vang", unit: 'Cái', price: 3100000, discounted: 0, rawAmount: 3100000, amount: 4, total: 3100000 },
                    { key: 'S004', nameProduct: 'STAN SMITH SHOES', description: "size xs", unit: 'Cái', price: 2300000, discounted: 0, rawAmount: 2300000, amount: 4, total: 2300000 },
                ]
            },
            {
                key: '5',
                code: 'DH005',
                postingDate: '05/03/2020',
                prepayment: 200000000,
                customer: customers[1],
                note: 'Giao hàng cẩn thận, hóa đơn đầy đủ',
                total: 171000000,
                paymentType: constant.paymentType[0],
                discounted: true,
                status: constant.status[0],
                products: [
                    { key: 'S001', nameProduct: 'Ultraboost 20 Shoes', description: "Mau vang, size s, phien ban gioi han", unit: 'Cái', price: 5000000, discounted: 10, rawAmount: 100000000, amount: 26, total: 90000000 },
                    { key: 'S002', nameProduct: 'STAR WARS NMD_R1 V2 SHOES', description: "Mau den", unit: 'Cái', price: 4500000, discounted: 30, rawAmount: 108000000, amount: 12, total: 75600000 },
                    { key: 'S003', nameProduct: 'ZX 8000 LEGO SHOES', description: "Ma vang", unit: 'Cái', price: 3100000, discounted: 0, rawAmount: 3100000, amount: 5, total: 3100000 },
                    { key: 'S004', nameProduct: 'STAN SMITH SHOES', description: "size xs", unit: 'Cái', price: 2300000, discounted: 0, rawAmount: 2300000, amount: 5, total: 2300000 },
                ]
            },
            {
                key: '7',
                code: 'DH007',
                postingDate: '09/02/2020',
                prepayment: 200000000,
                customer: customers[3],
                note: 'Giao hàng cẩn thận, hóa đơn đầy đủ',
                total: 171000000,
                paymentType: constant.paymentType[1],
                discounted: true,
                status: constant.status[1],
                products: [
                    { key: 'S001', nameProduct: 'Ultraboost 20 Shoes', description: "Mau vang, size s, phien ban gioi han", unit: 'Cái', price: 5000000, discounted: 20, rawAmount: 100000000, amount: 10, total: 90000000 },
                    { key: 'S002', nameProduct: 'STAR WARS NMD_R1 V2 SHOES', description: "Mau den", unit: 'Cái', price: 4500000, discounted: 30, rawAmount: 108000000, amount: 24, total: 75600000 },
                    { key: 'S003', nameProduct: 'ZX 8000 LEGO SHOES', description: "Ma vang", unit: 'Cái', price: 3100000, discounted: 0, rawAmount: 3100000, amount: 1, total: 3100000 },
                    { key: 'S004', nameProduct: 'STAN SMITH SHOES', description: "size xs", unit: 'Cái', price: 2300000, discounted: 0, rawAmount: 2300000, amount: 1, total: 2300000 },
                ]
            },
            {
                key: '8',
                code: 'DH008',
                postingDate: '10/01/2020',
                prepayment: 200000000,
                customerName: customers[0].name,
                customer: customers[0],
                note: 'Giao hàng cẩn thận, hóa đơn đầy đủ',
                total: 171000000,
                paymentType: constant.paymentType[2],
                discounted: true,
                status: constant.status[0],
                products: [
                    { key: 'S001', nameProduct: 'Ultraboost 20 Shoes', description: "Mau vang, size s, phien ban gioi han", unit: 'Cái', price: 5000000, discounted: 40, rawAmount: 100000000, amount: 20, total: 90000000 },
                    { key: 'S002', nameProduct: 'STAR WARS NMD_R1 V2 SHOES', description: "Mau den", unit: 'Cái', price: 4500000, discounted: 30, rawAmount: 108000000, amount: 14, total: 75600000 },
                    { key: 'S003', nameProduct: 'ZX 8000 LEGO SHOES', description: "Ma vang", unit: 'Cái', price: 3100000, discounted: 0, rawAmount: 3100000, amount: 1, total: 3100000 },
                    { key: 'S004', nameProduct: 'STAN SMITH SHOES', description: "size xs", unit: 'Cái', price: 2300000, discounted: 0, rawAmount: 2300000, amount: 1, total: 2300000 },
                ]
            },
            {
                key: '9',
                code: 'DH009',
                postingDate: '16/12/2020',
                prepayment: 200000000,
                customerName: customers[1].name,
                customer: customers[1],
                note: 'Giao hàng cẩn thận, hóa đơn đầy đủ',
                total: 171000000,
                paymentType: constant.paymentType[2],
                discounted: true,
                status: constant.status[3],
                products: [
                    { key: 'S001', nameProduct: 'Ultraboost 20 Shoes', description: "Mau vang, size s, phien ban gioi han", unit: 'Cái', price: 5000000, discounted: 5, rawAmount: 100000000, amount: 20, total: 90000000 },
                    { key: 'S002', nameProduct: 'STAR WARS NMD_R1 V2 SHOES', description: "Mau den", unit: 'Cái', price: 4500000, discounted: 30, rawAmount: 108000000, amount: 24, total: 75600000 },
                    { key: 'S003', nameProduct: 'ZX 8000 LEGO SHOES', description: "Ma vang", unit: 'Cái', price: 3100000, discounted: 10, rawAmount: 3100000, amount: 1, total: 3100000 },
                    { key: 'S004', nameProduct: 'STAN SMITH SHOES', description: "size xs", unit: 'Cái', price: 2300000, discounted: 20, rawAmount: 2300000, amount: 1, total: 2300000 },
                ]
            },
            {
                key: '10',
                code: 'DH0010',
                postingDate: '03/06/2020',
                prepayment: 200000000,
                customerName: customers[2].name,
                customer: customers[2],
                note: 'Giao hàng cẩn thận, hóa đơn đầy đủ',
                total: 171000000,
                paymentType: constant.paymentType[1],
                discounted: true,
                status: constant.status[0],
                products: [
                    { key: 'S001', nameProduct: 'Ultraboost 20 Shoes', description: "Mau vang, size s, phien ban gioi han", unit: 'Cái', price: 5000000, discounted: 10, rawAmount: 100000000, amount: 20, total: 90000000 },
                    { key: 'S002', nameProduct: 'STAR WARS NMD_R1 V2 SHOES', description: "Mau den", unit: 'Cái', price: 4500000, discounted: 30, rawAmount: 108000000, amount: 24, total: 75600000 },
                    { key: 'S003', nameProduct: 'ZX 8000 LEGO SHOES', description: "Ma vang", unit: 'Cái', price: 3100000, discounted: 0, rawAmount: 3100000, amount: 1, total: 3100000 },
                    { key: 'S004', nameProduct: 'STAN SMITH SHOES', description: "size xs", unit: 'Cái', price: 2300000, discounted: 0, rawAmount: 2300000, amount: 1, total: 2300000 },
                ]
            },
            {
                key: '11',
                code: 'DH0011',
                postingDate: '04/08/2020',
                prepayment: 200000000,
                customerName: customers[3].name,
                customer: customers[3],
                note: 'Giao hàng cẩn thận, hóa đơn đầy đủ',
                total: 171000000,
                paymentType: constant.paymentType[0],
                discounted: true,
                status: constant.status[0],
                products: [
                    { key: 'S001', nameProduct: 'Ultraboost 20 Shoes', description: "Mau vang, size s, phien ban gioi han", unit: 'Cái', price: 5000000, discounted: 10, rawAmount: 100000000, amount: 20, total: 90000000 },
                    { key: 'S002', nameProduct: 'STAR WARS NMD_R1 V2 SHOES', description: "Mau den", unit: 'Cái', price: 4500000, discounted: 30, rawAmount: 108000000, amount: 24, total: 75600000 },
                    { key: 'S003', nameProduct: 'ZX 8000 LEGO SHOES', description: "Ma vang", unit: 'Cái', price: 3100000, discounted: 0, rawAmount: 3100000, amount: 1, total: 3100000 },
                    { key: 'S004', nameProduct: 'STAN SMITH SHOES', description: "size xs", unit: 'Cái', price: 2300000, discounted: 0, rawAmount: 2300000, amount: 1, total: 2300000 },
                ]
            },
        ]
    },
    reducers: {
        getOrderPayment: (state, action) => {
            const { orderPayment } = action.payload;

            state.orderPayment = orderPayment;
        },
        setDetailOrder: (state, action) => {
            const { detailOrder } = action.payload;

            state.detailOrder = detailOrder;
        },
        increment: state => {
            // Redux Toolkit allows us to write "mutating" logic in reducers. It
            // doesn't actually mutate the state because it uses the Immer library,
            // which detects changes to a "draft state" and produces a brand new
            // immutable state based off those changes
            state.value += 1;
        },
        decrement: state => {
            state.value -= 1;
        },
        incrementByAmount: (state, action) => {
            state.value += action.payload;
        },
    },
});

export const { setDetailOrder } = saleOrdersSlice.actions;

// The function below is called a thunk and allows us to perform async logic. It
// can be dispatched like a regular action: `dispatch(incrementAsync(10))`. This
// will call the thunk with the `dispatch` function as the first argument. Async
// code can then be executed and other actions can be dispatched
export const incrementAsync = amount => dispatch => {
    setTimeout(() => {
        dispatch(incrementByAmount(amount));
    }, 1000);
};

// The function below is called a selector and allows us to select a value from
// the state. Selectors can also be defined inline where they're used instead of
// in the slice file. For example: `useSelector((state) => state.counter.value)`
export const selectDetailOrder = state => state.saleOrder.detailOrder;
export const selectOrders = state => state.saleOrder.orders;

export default saleOrdersSlice.reducer;
