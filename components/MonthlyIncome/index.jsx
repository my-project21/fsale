// Libaries
import React, { useEffect, useMemo, useRef, useState } from 'react';
import {
    Layout,
    Input,
    Table,
    Row,
    Col,
    Tabs,
    Empty,
    Button,
    Select,
    Divider,
    InputNumber,
    Popover,
    Modal,
    Typography,
    DatePicker
} from 'antd';
import numeral from 'numeral'
import moment from 'moment'
import {
    LineChart, AreaChart, Area, XAxis, YAxis, CartesianGrid, Tooltip, Legend,
} from 'recharts';


const { Header, Content, Footer } = Layout;
const { Title, Text } = Typography
// Styles
import styles from './styles.module.scss';
import { format } from 'numeral';

const Charts = (props) => {
    const { products = [] } = props;

    const dataAo = useMemo(() => {
        const data = [];
        for (let i = 0; i < 12; i++) {
            let object = { name: `Tháng ${i + 1}` };

            ['Áo', 'Quần', 'Giày', 'Dép'].map(name => {
                object = { ...object, [name]: Math.floor(Math.random() * 100000000) + 20000000 }
            })

            data.push(object)
        }

        return data;
    }, [products])

    const CustomTooltip = ({ active, payload, label }) => {
        return payload ? payload.map(item => {
            return (
                <div style={{ color: item.fill, background: '#fff', padding: 10 }}>
                    <strong>{item.dataKey}</strong> &nbsp;
                    <span>{numeral(item.value).format('0,0.0')} vnd</span>
                </div>
            )
        }) : <div>no data</div>
    };

    return (
        <div style={{ padding: '0px 180px' }}>
            <Title>Biểu đồ</Title>
            <div className='d-flex j-c' style={{ justifyContent: 'center' }}>
                <AreaChart
                    width={1300}
                    height={500}
                    data={dataAo}
                    margin={{
                        top: 5, right: 30, left: 20, bottom: 5,
                    }}
                >
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis dataKey="name" />
                    <YAxis label={{ value: 'Số tiền', angle: -90, position: 'insideLeft' }}>
                    </YAxis>
                    <Tooltip content={<CustomTooltip />} />
                    <Legend />
                    {['Áo', 'Quần', 'Giày', 'Dép'].map(name => {
                        const randomColor = "#" + Math.floor(Math.random() * 16777215).toString(16);
                        return (
                            <Area key={name} type='monotone' fill={randomColor} dataKey={name} stroke={randomColor}></Area>
                        )
                    })}
                    {/* <Line type="monotone" dataKey="pv" stroke="#8884d8" activeDot={{ r: 8 }} />
                    <Line type="monotone" dataKey="uv" stroke="#82ca9d" /> */}
                </AreaChart>
            </div>
        </div>
    )
}

const SaleManager = (props) => {
    // State
    const [filter, setFilter] = useState({
        label: '',
        distributor: '',
        customer: '',
        origin: '',
        transport: '',
    })
    const [currency, setCurrency] = useState('vnd')


    // Variable
    const columns = [
        {
            title: 'ID sản phẩm',
            dataIndex: 'code',
            key: 'code',
        },
        {
            title: 'Tên sản phẩm',
            dataIndex: 'label',
            key: 'label',
        },
        {
            title: 'Nhà phân phối',
            dataIndex: 'distributor',
            key: 'distributor',
            render: (distributor) => {
                return (
                    <>
                        {distributor.name}
                    </>
                )
            }
        },
        {
            title: 'Khách hàng',
            dataIndex: 'customer',
            key: 'customer',
            render: (customer) => {
                return (
                    <>
                        {customer.name}
                    </>
                )
            }
        },
        {
            title: 'Xuất xứ',
            dataIndex: 'origin',
            key: 'origin',
            render: (origin) => {
                return (
                    <>
                        {origin.name}
                    </>
                )
            }
        },
        {
            title: 'Tên công ty vận chuyển',
            dataIndex: 'transport',
            key: 'transport',
            render: (transport) => {
                return (
                    <>
                        {transport.name}
                    </>
                )
            }
        },
        {
            title: 'Số lượng',
            dataIndex: 'amount',
            key: 'amount',
        },
        {
            title: 'Tổng tiền',
            dataIndex: 'total',
            key: 'total',
            render: (_, record) => {
                let total = record.amount * record.price;

                if (currency === '$') {
                    total = total / 23000;
                }

                return (
                    <>
                        {numeral(total).format('0,0')} {currency}
                    </>
                )
            }
        },
    ]

    const distributors = [
        { key: '1ffa', name: 'Công ty xuất nhập khẩu quần áo Adidas' },
        { key: '10', name: 'Công ty Ariel shop sỉ lẻ quần áo nike' },
    ]

    const customers = [
        { id: 'customer-1', name: 'Nguyễn Thị Thập', phone: '0982787652' },
        { id: 'customer-2', name: 'Trần Bá Phước', phone: '0989747652' },
        { id: 'customer-3', name: 'Lưu Thị Lý', phone: '0989787612' },
        { id: 'customer-4', name: 'Ưng Hoàng Kha', phone: '0989797652' },
    ]

    const origins = [
        { key: '0', name: 'Trung quốc' },
        { key: '1', name: 'Việt Nam' },
        { key: '2', name: 'Ấn Độ' },
        { key: '3', name: 'Mỹ' },
    ]

    const transports = [
        { key: '1', name: 'Dương Minh Logistics' },
        { key: '2', name: 'Công ty TNHH TM & DVVT Hoa Lâm' },
        { key: '3', name: 'Công ty TNHH DV VT Trọng Tấn' },
        { key: '4', name: 'Viettel Post' },
        { key: '5', name: 'Việt Nam Spot (VNpost / EMS)' },
    ]

    const [products, setProducts] = useState([
        { key: '1', distributor: distributors[0], code: 'S001', customer: customers[0], origin: origins[0], transport: transports[0], label: 'Giày adidas superstar trắng', img: 'images/ultraboost_20.jpg', price: 2400000, amount: 20 },
        { key: '131', distributor: distributors[0], code: 'S001', customer: customers[1], origin: origins[0], transport: transports[0], label: 'Giày adidas superstar trắng', img: 'images/ultraboost_20.jpg', price: 2400000, amount: 20 },
        { key: '1r12', distributor: distributors[0], code: 'S001', customer: customers[2], origin: origins[0], transport: transports[0], label: 'Giày adidas superstar trắng', img: 'images/ultraboost_20.jpg', price: 2400000, amount: 20 },
        { key: '2qwr', distributor: distributors[0], code: 'S002', customer: customers[2], origin: origins[0], transport: transports[0], label: 'Giày adidas superstar đen', img: 'images/star_wars_nmd_r1.jpg', price: 2400000, amount: 24 },
        { key: '3', distributor: distributors[0], code: 'S003', customer: customers[0], origin: origins[0], transport: transports[1], label: 'Giày adidas YEEZY BOOST 380', img: 'images/ZX_8000_LEGO_Shoes_Yellow_FZ3482_01_standard.jpg', price: 6300000, amount: 20 },
        { key: '4', distributor: distributors[0], code: 'S004', customer: customers[0], origin: origins[0], transport: transports[1], label: 'Giày adidas ULTRABOOST 20 DNA', img: 'images/Stan_Smith_Shoes_White_M20324_01_standard.jpg', price: 5000000, amount: 10 },
        { key: '5', distributor: distributors[0], code: 'C001', customer: customers[0], origin: origins[0], transport: transports[1], label: 'DÉP ADIDAS QUAI NGANG ADILETTE BOOST', img: 'images/Human_Made_Short_Sleeve_Tee_White_GM4255_01_laydown.jpg', price: 1500000, amount: 12 },
        { key: '6', distributor: distributors[0], code: 'C002', customer: customers[2], origin: origins[0], transport: transports[1], label: 'DÉP ADIDAS QUAI NGANG ADILETTE AQUA', img: 'images/Human_Made_Sweatshirt_Blue_GM4268_01_laydown.jpg', price: 4800000, amount: 0 },
        { key: '7', distributor: distributors[0], code: 'C003', customer: customers[0], origin: origins[0], transport: transports[0], label: 'DÉP ADIDAS QUAI NGANG ADILETTE COMFORT', img: 'images/UB_Colorblock_Jacket_Blue_GL0401_01_laydown.jpg', price: 800000, amount: 90 },
        { key: '8', distributor: distributors[0], code: 'C005', customer: customers[1], origin: origins[0], transport: transports[0], label: 'ÁO THUN PRIMEBLUE', img: 'images/Own_the_Run_Two_in_One_Shorts_Blue_GC7882_01_laydown.jpg', price: 660000, amount: 10 },
        { key: '9', distributor: distributors[0], code: 'C006', customer: customers[1], origin: origins[0], transport: transports[0], label: 'ÁO THUN ADIDAS Z.N.E.', img: 'images/Own_the_Run_Two_in_One_Shorts_Blue_GC7882_01_laydown.jpg', price: 880000, amount: 200 },
        { key: '10', code: 'C007', distributor: distributors[0], customer: customers[2], origin: origins[0], transport: transports[1], label: 'ÁO POLO 3 SỌC BASIC', img: 'images/Own_the_Run_Two_in_One_Shorts_Blue_GC7882_01_laydown.jpg', price: 906500, amount: 40 },
        { key: '11', code: 'C008', distributor: distributors[0], customer: customers[1], origin: origins[0], transport: transports[0], label: 'ÁO POLO ADIDAS GOLF', img: 'images/Own_the_Run_Two_in_One_Shorts_Blue_GC7882_01_laydown.jpg', price: 1200000, amount: 400 },
        { key: '12', code: 'C009', distributor: distributors[0], customer: customers[0], origin: origins[0], transport: transports[2], label: 'QUẦN SHORT VẢI DOBBY 3 SỌC', img: 'images/Own_the_Run_Two_in_One_Shorts_Blue_GC7882_01_laydown.jpg', price: 1396500, amount: 70 },
        { key: '13', code: 'C0010', distributor: distributors[0], customer: customers[2], origin: origins[1], transport: transports[2], label: 'QUẦN SHORT GRAPHIC', img: 'images/Own_the_Run_Two_in_One_Shorts_Blue_GC7882_01_laydown.jpg', price: 600000, amount: 40 },
        { key: '14', code: 'C0011', distributor: distributors[0], customer: customers[0], origin: origins[1], transport: transports[2], label: 'QUẦN SHORT KẺ SỌC NHỎ ULTIMATE365', img: 'images/Own_the_Run_Two_in_One_Shorts_Blue_GC7882_01_laydown.jpg', price: 1256500, amount: 90 },
        { key: '15', code: 'C0012', distributor: distributors[0], customer: customers[1], origin: origins[1], transport: transports[2], label: 'ÁO POLO ADIPURE IN HỌA TIẾT', img: 'images/Own_the_Run_Two_in_One_Shorts_Blue_GC7882_01_laydown.jpg', price: 1396500, amount: 0 },
        { key: '16', code: 'C0013', distributor: distributors[1], customer: customers[1], origin: origins[1], transport: transports[2], label: 'GIÀY Nike Air Zoom Pegasus 37', img: 'images/Own_the_Run_Two_in_One_Shorts_Blue_GC7882_01_laydown.jpg', price: 3519000, amount: 30 },
        { key: '17', code: 'C0014', distributor: distributors[1], customer: customers[2], origin: origins[1], transport: transports[0], label: 'Giày nike air force 1', img: 'images/Own_the_Run_Two_in_One_Shorts_Blue_GC7882_01_laydown.jpg', price: 3519000, amount: 60 },
        { key: '18', code: 'C0015', distributor: distributors[1], customer: customers[2], origin: origins[1], transport: transports[0], label: 'Giày nike air max 90', img: 'images/Own_the_Run_Two_in_One_Shorts_Blue_GC7882_01_laydown.jpg', price: 3519000, amount: 111 },
        { key: '19', code: 'C0016', distributor: distributors[1], customer: customers[1], origin: origins[1], transport: transports[0], label: 'Giày nike sb zoom blazer', img: 'images/Own_the_Run_Two_in_One_Shorts_Blue_GC7882_01_laydown.jpg', price: 2929000, amount: 37 },
        { key: '20', code: 'C0017', distributor: distributors[1], customer: customers[0], origin: origins[1], transport: transports[0], label: 'Áo khoác nike sportswear', img: 'images/Own_the_Run_Two_in_One_Shorts_Blue_GC7882_01_laydown.jpg', price: 1739000, amount: 97 },
        { key: '21', code: 'C0018', distributor: distributors[1], customer: customers[1], origin: origins[1], transport: transports[0], label: 'Áo khoác Nike Throwback', img: 'images/Own_the_Run_Two_in_One_Shorts_Blue_GC7882_01_laydown.jpg', price: 2929000, amount: 57 },
        { key: '22', code: 'C0019', distributor: distributors[1], customer: customers[2], origin: origins[1], transport: transports[0], label: 'Quần short jordan jumpman classics', img: 'images/Own_the_Run_Two_in_One_Shorts_Blue_GC7882_01_laydown.jpg', price: 1123000, amount: 47 },
        { key: '24', code: 'C0020', distributor: distributors[1], customer: customers[2], origin: origins[1], transport: transports[0], label: 'Giày nike sb zoom blazer', img: 'images/Own_the_Run_Two_in_One_Shorts_Blue_GC7882_01_laydown.jpg', price: 2929000, amount: 27 },
    ])

    let newProducts = useMemo(() => {
        let newProducts = [...products];

        newProducts = products.filter(product => {
            let isLabel = product.label.toLowerCase().indexOf(filter.label.toLowerCase()) !== -1;
            let isOrigin = product.origin.key.toLowerCase().indexOf(filter.origin) !== -1;
            let isDistributor = product.distributor.key.toLowerCase().indexOf(filter.distributor) !== -1;
            let isCustomer = product.customer.id.toLowerCase().indexOf(filter.customer) !== -1;
            let isTransport = product.transport.key.toLowerCase().indexOf(filter.transport) !== -1;

            if (isLabel && isOrigin && isDistributor && isCustomer && isTransport) {
                return true;
            }

            return false;
        })

        return newProducts;
    }, [filter])

    const total = useMemo(() => {
        const myTotal = newProducts.reduce((accumulator, currentValue) => {
            const total = currentValue.price * currentValue.amount;

            return accumulator + total;
        }, 0)

        return myTotal;

    }, [newProducts])

    const onChangeFilter = (value, type) => {
        setFilter({
            ...filter,
            [type]: value
        })
    }

    const setDefaultFilter = () => {
        setFilter({
            label: '',
            distributor: '',
            customer: '',
            origin: '',
            transport: '',
        })
    }

    const renderInfo = () => {
        return (
            <div style={{ padding: '0px 30px' }}>
                <div className="d-flex j-center" style={{ marginBottom: '10px', gap: 50 }}>
                    <div className="d-flex gap-5 a-center">
                        <div style={{ fontSize: 15, fontWeight: 'bold', color: '#51cff8' }}>Ngày bắt đầu</div>
                        <DatePicker picker="month" defaultValue={moment().subtract(1, 'month')} format='DD-MM-YYYY' />
                    </div>
                    <div className="d-flex gap-5 a-center">
                        <div style={{ fontSize: 15, fontWeight: 'bold', color: '#51cff8' }}>Ngày kết thúc</div>
                        <DatePicker picker="month" defaultValue={moment()} format='DD-MM-YYYY' />
                    </div>
                </div>
                <Title level={4}>Thông tin sản phẩm</Title>
                <div className={styles['group__box']}>
                    <Row gutter={[20, 20]} >
                        <Col span={12}>
                            <Input value={filter.label} placeholder='Tên sản phẩm' onChange={(event) => onChangeFilter(event.target.value, 'label')} />
                        </Col>
                        <Col span={12}>
                            <DatePicker style={{ width: '100%' }} placeholder="Chọn ngày sản xuất" />
                        </Col>
                    </Row>
                    <Row gutter={[20, 20]} >
                        <Col span={12}>
                            <Select value={filter.distributor !== '' ? filter.distributor : null} placeholder="Nhà phân phối" style={{ width: '100%' }} onChange={(value) => onChangeFilter(value, 'distributor')}>
                                {distributors.map(distributor => {
                                    return (
                                        <Select.Option key={distributor.key} value={distributor.key}>{distributor.name}</Select.Option>
                                    )
                                })}
                            </Select>
                        </Col>
                        <Col span={12}>
                            <Select value={filter.origin !== '' ? filter.origin : null} placeholder="Xuất xứ" style={{ width: '100%' }} onChange={(value) => onChangeFilter(value, 'origin')}>
                                {origins.map(origin => {
                                    return (
                                        <Select.Option key={origin.key} value={origin.key}>{origin.name}</Select.Option>
                                    )
                                })}
                            </Select>
                        </Col>
                    </Row>
                    <Row gutter={[20, 20]} >
                        <Col span={12}>
                            <Select value={filter.customer !== '' ? filter.customer : null} placeholder="Tên khách hàng" style={{ width: '100%' }} onChange={(value) => onChangeFilter(value, 'customer')}>
                                {customers.map(custom => {
                                    return (
                                        <Select.Option key={custom.id} value={custom.id}>{custom.name}</Select.Option>
                                    )
                                })}
                            </Select>
                        </Col>
                        <Col span={12}>
                            <Select placeholder="Tên công ty vận chuyển" style={{ width: '100%' }} onChange={(value) => onChangeFilter(value, 'transport')}>
                                {transports.map(transport => {
                                    return (
                                        <Select.Option key={transport.key} value={transport.key}>{transport.name}</Select.Option>
                                    )
                                })}
                            </Select>
                        </Col>
                    </Row>
                </div>
                <Row style={{ margin: '10px 0px' }} gutter={10}>
                    <Col>
                        <strong>Đơn vị tiền tệ</strong>  &nbsp;
                        <Select defaultValue='vnd' onChange={(value) => setCurrency(value)}>
                            <Select.Option value='vnd'>VND</Select.Option>
                            <Select.Option value='$'>USD</Select.Option>
                        </Select>
                    </Col>
                    <Col>
                        <Button type='primary' onClick={setDefaultFilter}>Đặt lại nhanh</Button>
                    </Col>
                </Row>
                <Table
                    style={{ marginTop: 10 }}
                    scroll={{ y: 240 }}
                    bordered
                    size='small'
                    columns={columns}
                    dataSource={newProducts}
                    footer={() => (
                        <div>
                            <strong>Tổng cộng:</strong> &nbsp;
                            <span>{currency === 'vnd' ? `${numeral(total).format('0,0.0')} vnd` : `${numeral(total / 23000).format('0,0.0')} $`}</span>
                        </div>
                    )}
                />
            </div>
        )
    }

    const renderChart = () => {
        return (
            <div>
                <Charts products={products} />
            </div>
        )
    }


    const tabs = [
        { key: 'info-product', label: 'Thông tin sản phẩm', content: renderInfo() },
        { key: 'chart-product', label: 'Biểu đồ', content: renderChart() },
    ]

    return (
        <Layout>
            <Content className={styles['main-content']}>
                <Tabs
                    className={styles['main-content__tabs']}
                    tabBarStyle={{ background: '#36cfc9', height: 60 }}
                    type='card'
                >
                    {tabs.map(tab => {
                        return (
                            <Tabs.TabPane key={tab.key} tab={tab.label}>
                                {tab.content}
                            </Tabs.TabPane>
                        )
                    })}
                </Tabs>
            </Content>
        </Layout>
    );
};

export default SaleManager;



