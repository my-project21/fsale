import { configureStore } from '@reduxjs/toolkit';
import saleManagerReducer from '../components/SaleManager/saleManagerSlice';
import saleOrderReducer from '../components/SaleOrders/saleOrdersSlice';

export default configureStore({
    reducer: {
        saleManager: saleManagerReducer,
        saleOrder: saleOrderReducer
    },
});
