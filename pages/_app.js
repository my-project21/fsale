// Libraries
import { Provider } from 'react-redux'
import store from '../app/store';

// Assets
import 'antd/dist/antd.css';
import '../assets/global.scss';

import 'vui-components/antsomi.css'
export default function App({ Component, pageProps }) {
	return (
		<Provider store={store}>
			<Component {...pageProps} />
		</Provider>
	)
}
