// Libraries
import React, { useEffect } from 'react';
import { useRouter } from 'next/router'
import Icon from 'vui-components/antsomi';
// Components
import SaleManager from '../components/SaleManager';

const Index = () => {
    const router = useRouter();

    useEffect(() => {
        router.push('/NewOrder')
    }, [])

    return (
        <SaleManager />
    )
};

Index.propTypes = {};

export default Index;