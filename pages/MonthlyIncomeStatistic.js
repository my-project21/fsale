import React from 'react';
import PropTypes from 'prop-types';
import MonthlyIncome from '../components/MonthlyIncome';

const MonthlyIncomeStatistic = () => {
    return (
        <div>
            <MonthlyIncome />
        </div>
    );
};

MonthlyIncomeStatistic.propTypes = {};

export default MonthlyIncomeStatistic;