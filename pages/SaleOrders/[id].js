import React, { useState, useEffect, useMemo } from 'react';
import PropTypes from 'prop-types';
import { Layout, Typography, Modal, message, Input, Button, Table, Form, Row, Col, InputNumber, Select, Space, searchInput, notification } from 'antd';
import Link from 'next/link'
import numeral from 'numeral';
import {isEmpty, isEqual} from 'lodash'

// Icons
import { HomeOutlined, MenuOutlined, ExclamationCircleOutlined, PlusOutlined, EditOutlined, FileTextOutlined, DeleteOutlined, ShoppingCartOutlined, FilterOutlined } from '@ant-design/icons';

// Antd
const { Header, Content, Footer } = Layout;
const { Title, Text } = Typography;

import styles from './styles.module.scss';

import { useRouter } from 'next/router'
import TextArea from 'antd/lib/input/TextArea';

// Redux
import { selectOrders } from '../../components/SaleOrders/saleOrdersSlice'
import { useSelector } from 'react-redux';

const constant = {
    paymentType: ['Ngân hàng', 'COD', 'Tiền mặt'],
    status: [{ label: 'Đã giao', color: '#52c41a' }, { label: 'Đã thanh toán', color: '#52c41a' }, { label: 'Đang chờ', color: '#13c2c2' }, { label: 'Đang giao', color: '#13c2c2' }]
}

export function getServerSideProps(context) {
    return {
      props: {params: context.params}
    };
  }

const SaleOrderDetail = (props) => {
    // Props
    const {params} = props; 

    // State
    const [form] = Form.useForm();
    const router = useRouter()
    const [products, setProducts] = useState([
        { key: 'S001', nameProduct: 'Ultraboost 20 Shoes', description: "Mau vang, size s, phien ban gioi han", unit: 'Cai', price: 5000000, discounted: 10, rawAmount: 100000000, amount: 20, total: 90000000 },
        { key: 'S002', nameProduct: 'STAR WARS NMD_R1 V2 SHOES', description: "Mau den", unit: 'Cai', price: 4500000, discounted: 30, rawAmount: 108000000, amount: 24, total: 75600000 },
        { key: 'S003', nameProduct: 'ZX 8000 LEGO SHOES', description: "Ma vang", unit: 'Cai', price: 3100000, discounted: 0, rawAmount: 3100000, amount: 1, total: 3100000 },
        { key: 'S004', nameProduct: 'STAN SMITH SHOES', description: "size xs", unit: 'Cai', price: 2300000, discounted: 0, rawAmount: 2300000, amount: 1, total: 2300000 },
    ])
    const [detailOrder, setDetailOrder] = useState({});
    const [changeDetailOrder, setChangeDetailOrder] = useState({})
    const orders = useSelector(selectOrders);

    const { id } = router.query

    useState(() => {
        const selectedOrder = orders.find(order => order.code === params.id)
        console.log("🚀 ~ file: [id].js ~ line 56 ~ useState ~ selectedOrder", selectedOrder)

        if (selectedOrder) {
            const total = selectedOrder.products.reduce((accumulator, currentValue) => {
                const total = currentValue.price * currentValue.amount;
                const discounted = currentValue.discounted * total / 100;

                const final = total - discounted;

                return accumulator + final;
            }, 0)

            const newOder = {
                ...selectedOrder,
                total
            }

            setDetailOrder(newOder)
        }
    }, [orders, params])

    const draftProducts = useMemo(() => {
        if (detailOrder.products) {
            return detailOrder.products.map((product) => {
                const rawAmount = product.price * product.amount;
                const total = product.price * product.amount;
                const discounted = product.discounted * total / 100;
    
                const final = total - discounted;
                return (
                    {
                        ...product,
                        total: final,
                        rawAmount
                    }
                )
            })
        }
    }, [detailOrder])

    useEffect(() => {
        if (products.length) {
            const total = products.reduce((accumulator, currentValue) => {
                const total = currentValue.price * currentValue.amount;
                const discounted = currentValue.discounted * total / 100;

                const final = total - discounted;

                return accumulator + final;
            }, 0)

            const newOder = {
                ...detailOrder,
                total
            }

            setDetailOrder(newOder)
        }
    }, [products]);

    useEffect(() => {
        if (detailOrder.products) {
            const newProducts = detailOrder.products.map((product) => {
                const rawAmount = product.price * product.amount;
                const total = product.price * product.amount;
                const discounted = product.discounted * total / 100;
    
                const final = total - discounted;
         
                return (
                    {
                        ...product,
                        total: final,
                        rawAmount
                    }
                )
            })

            setProducts(newProducts)
        }
    }, [detailOrder.products]);

    useEffect(() => {
        if (detailOrder.customer) {
            const newDetailOrder = {...detailOrder, customerName: detailOrder.customer.name, address: detailOrder.customer.address, status: detailOrder.status.label, phone: detailOrder.customer.phone, email: detailOrder.customer.email}
            form.setFieldsValue(newDetailOrder)
        }
    }, [detailOrder]);

    let isDisabledEdit = useMemo(() => {
        if (!isEmpty(detailOrder) && !isEmpty(changeDetailOrder)) {
            const oldDetail = {paymentType: detailOrder.paymentType, prepayment: detailOrder.prepayment, status: detailOrder.status.label }
            const newDetail = {paymentType: changeDetailOrder.paymentType, prepayment: changeDetailOrder.prepayment, status: changeDetailOrder.status }

            if (isEqual(oldDetail, newDetail)) {
                return true;
            }

            return false;
        }

        return true

    }, [detailOrder, changeDetailOrder])

    const onFinish = () => {

    }

    const formItemLayout = {
        labelCol: {
            xs: { span: 24 },
            sm: { span: 7 },
        },
        wrapperCol: {
            xs: { span: 24 },
            sm: { span: 17 },
        },
    };

    const onChangeAmount = (index, value) => {
        const draftProducts = [...products]

        draftProducts[index].amount = value;

        setProducts(draftProducts)
    }

    const columns = [
        {
            title: 'Sản phẩm',
            dataIndex: 'nameProduct',
            key: 'nameProduct',
            render: text => <a>{text}</a>,
        },
        {
            title: 'Mô tả',
            dataIndex: 'description',
            key: 'description',
        },
        {
            title: 'Số lượng',
            dataIndex: 'amount',
            key: 'amount',
            render: (value, _, index) => {
              return (
                <InputNumber defaultValue={value} onChange={(value) => onChangeAmount(index, value)}>
                </InputNumber>
              )
            }
        },
        {
            title: 'Đơn vị',
            key: 'unit',
            dataIndex: 'unit',
        },
        {
            title: 'Giá',
            key: 'price',
            dataIndex: 'price',
            render: (money) => <div>{numeral(money).format('0,0')} đ</div>
        },
        {
            title: 'Tạm tính',
            key: 'rawAmount',
            dataIndex: 'rawAmount',
            render: (money, record) => {
                const raw = record.price * record.amount;

               return <div>{numeral(raw).format('0,0')} đ</div>
            }
        },
        {
            title: 'Giảm giá',
            key: 'discounted',
            dataIndex: 'discounted',
            render: (percent) => <div>{percent}%</div>
        },
        {
            title: 'Tổng cộng',
            key: 'total',
            dataIndex: 'total',
            render: (money, record) => {
                const total = record.price * record.amount;
                const discounted = record.discounted * total / 100;
    
                const final = total - discounted;

               return <div>{numeral(final).format('0,0')} đ</div>
            }
        },
    ];

    const onValuesChange = (_, allValue) => {
        setChangeDetailOrder(allValue)
    }

    const onClickSave = () => {
        Modal.confirm({
            title: 'Cập nhật đơn hàng',
            icon: <ExclamationCircleOutlined />,
            content: 'Bạn có muốn cập nhật lại đơn hàng không',
            onOk() {
                message.success('Cập nhật lại đơn hàng thành công');
                setDetailOrder(changeDetailOrder)
            },
            onCancel() {},
          });
    }

    const onClickDelete = () => {
        Modal.confirm({
            title: 'Xóa đơn hàng',
            content: `Bạn có muốn xóa đơn hàng ${id}`,
            onOk: () => {
                  router.push('/SaleOrders')
                  notification.success({
                    message: 'Xóa đơn hàng',
                    description: 'Xóa đơn hàng thành công',
                  })

            }
        })
    }

    return (
        <Layout className={'Sale-orders__wrap'}>
            <Header className={styles['Layout__header']} >
                <Link href='/'><HomeOutlined style={{ fontSize: 25 }} /></Link>
                <div className={styles['wrap__options']}>
                    <div className="d-flex gap-5">
                        <Link href={`/NewOrder?customerId=${detailOrder.customer ? detailOrder.customer.id : 'customer-1'}`}>
                        <Button shape="circle" size={'large'} className={styles['options__button']} icon={<PlusOutlined />} />
                        </Link>
                        {/* <Button shape="circle" size={'large'} className={`${styles['options__button']} ${isDisabledEdit ? styles['disabled'] : null}`} icon={<EditOutlined />} /> */}
                        <Button onClick={onClickSave} disabled={isDisabledEdit} shape="circle" size={'large'} className={`${styles['options__button']} ${isDisabledEdit ? styles['disabled'] : null}`} icon={<FileTextOutlined />} />
                        <Button onClick={onClickDelete} shape="circle" size={'large'} className={styles['options__button']} icon={<DeleteOutlined />} />
                    </div>
                    {/* <FilterOutlined style={{ fontSize: 25 }} /> */}
                </div>
                <MenuOutlined style={{ fontSize: 25 }} />
            </Header>
            <Content className={styles['Layout__content']}>
                <Title level={3}>Đơn hàng: {id}</Title>
                <section>
                    <Text strong>Thông tin chung</Text>
                    <div className={styles['general__information']}>
                        <Form
                            {...formItemLayout}
                            form={form}
                            name="general_information"
                            onFinish={onFinish}
                            onValuesChange={onValuesChange}
                            labelAlign='left'
                        >
                            <Row gutter={20}>
                                <Col span={9}>
                                    <Form.Item
                                        name={`customerName`}
                                        label={`Tên khác hàng`}

                                    >
                                        <Input placeholder="placeholder" />
                                    </Form.Item>
                                    <Form.Item
                                        name={`phone`}
                                        label={`Số điện thoại`}

                                    >
                                        <Input placeholder="placeholder" />
                                    </Form.Item>
                                    <Form.Item
                                        name={`email`}
                                        label={`Địa chỉ email`}

                                    >
                                        <Input placeholder="placeholder" />
                                    </Form.Item>
                                    <Form.Item
                                        name={`address`}
                                        label={`Địa chỉ`}

                                    >
                                        <Input placeholder="placeholder" />
                                    </Form.Item>
                                </Col>
                                <Col span={9}>
                                    <Form.Item
                                        name={`paymentType`}
                                        label={`Loại thanh toán`}

                                    >
                                         <Select>
                                            {constant.paymentType.map(item => {
                                                return (
                                                    <Select.Option key={item} value={item}>
                                                        {item}
                                                    </Select.Option>
                                                )
                                            })}
                                        </Select>
                                    </Form.Item>
                                    <Form.Item
                                        name={`prepayment`}
                                        label={`Số tiền trả trước`}

                                    >
                                        <InputNumber 
                                            style={{width: '100%'}}    
                                            placeholder="Nhập số tiền thanh toán"
                                            formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                            parser={value => value.replace(/(,*)/g, '')}
                                        />
                                    </Form.Item>
                                    <Form.Item
                                        name={`total`}
                                        label={`Tổng cộng`}

                                    >
                                        <InputNumber 
                                            style={{width: '100%'}}
                                            placeholder="placeholder" 
                                            formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                            parser={value => value.replace(/(,*)/g, '')}
                                        />
                                    </Form.Item>
                                    <Form.Item
                                        name={`status`}
                                        label={`Trạng thái`}
                                    >
                                        <Select>
                                            {constant.status.map(item => {
                                                return (
                                                    <Select.Option key={item.label} value={item.label}>
                                                        {item.label}
                                                    </Select.Option>
                                                )
                                            })}
                                        </Select>
                                    </Form.Item>
                                </Col>
                                <Col span={6}>
                                    <Form.Item
                                        labelCol={
                                            {
                                                xs: { span: 24 },
                                                sm: { span: 24 },
                                            }
                                        }
                                        wrapperCol={
                                            {
                                                xs: { span: 24 },
                                                sm: { span: 24 },
                                            }
                                        }
                                        name={`note`}
                                        label={`Ghi chú`}

                                    >
                                        <Input.TextArea rows={4} style={{fontWeight: 'bold'}} />
                                    </Form.Item>
                                </Col>
                            </Row>
                        </Form>
                    </div>
                </section>
                <section style={{ marginTop: 10 }}>
                    <Text strong>Danh sách sản phẩm</Text>
                    <Table
                        onRow={(record, index) => {
                            return {
                                className: index % 2 === 0 ? styles['row-even'] : null
                            }
                        }}
                        columns={columns}
                        dataSource={products}
                        size='small' />

                </section>
            </Content>
        </Layout >
    );
};

SaleOrderDetail.propTypes = {};

export default SaleOrderDetail;
