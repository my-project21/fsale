import React from 'react';
import PropTypes from 'prop-types';

import SaleOrders from '../../components/SaleOrders/index';

const PageSaleOrders = () => {
    return (
       <SaleOrders />
    );
};

PageSaleOrders.propTypes = {};

export default PageSaleOrders;