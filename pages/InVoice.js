// Libraries
import React, { useMemo } from 'react';
import { Row, Col, Space, Typography, Table, Button, notification } from 'antd'
import { useSelector } from 'react-redux'
import numeral from 'numeral';
import { useRouter } from 'next/router'
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';

// Redux toolkit
import { selectOrderPayment, selectInitialCategory } from '../components/SaleManager/saleManagerSlice';

// Antd
const { Title, Text } = Typography

const InVoice = () => {
    // Router
    const router = useRouter();

    // Redux toolkit
    const orderPayment = useSelector(selectOrderPayment)
    const initialCategory = useSelector(selectInitialCategory)
    const { infoOrder = {}, products = [] } = orderPayment;
    const { customer = {} } = infoOrder;

    const getProduct = (code) => {
        let newProduct = {};
        initialCategory.map(category => {
            const match = category.products.find(product => product.code === code);

            if (match) {
                newProduct = match
            }
        })

        return newProduct || {};
    }

    const newProducts = useMemo(() => {
        let draftProducts = [...products];

        draftProducts = draftProducts.map((product, index) => ({
            ...product,
            key: product.code,
            stt: index + 1,
            nameProduct: getProduct(product.code).label,
        }))
        console.log("🚀 ~ file: InVoice.js ~ line 41 ~ draftProducts=draftProducts.map ~ draftProducts", draftProducts)

        return draftProducts;
    }, [products])
    console.log("🚀 ~ file: InVoice.js ~ line 41 ~ newProducts ~ newProducts", newProducts)

    // Table
    const columns = [
        {
            title: 'Stt',
            dataIndex: 'stt',
            key: 'stt',
            align: 'center'
        },
        {
            title: 'Tên sản phẩm',
            dataIndex: 'nameProduct',
            key: 'nameProduct',
        },
        {
            title: 'Số lượng',
            dataIndex: 'amount',
            key: 'amount',
            align: 'center'
        },
        {
            title: 'Đơn giá',
            key: 'price',
            dataIndex: 'price',
            align: 'center',
            render: (text) => (
                <div >{numeral(text).format(0, 0)} vnđ</div>
            )
        },
        {
            title: 'Tổng tiền',
            dataIndex: 'total',
            key: 'total',
            align: 'end',
            render: (text) => (
                <Text strong style={{ color: '#08979c' }}>{numeral(text).format(0, 0)} vnđ</Text>
            )
        },
    ];

    const onClickSave = () => {
        notification.success({
            message: 'Lưu',
            description: 'Lưu đơn hàng thành công'
        })

        setTimeout(() => {
            router.push('/NewOrder')
        }, 1000)
    }

    // const PrintElem = (elem) => {
    //     var mywindow = window.open('', 'PRINT', 'height=400,width=600');

    //     mywindow.document.write('<html><head><title>' + document.title + '</title>');
    //     mywindow.document.write('</head><body >');
    //     mywindow.document.write('<h1>' + document.title + '</h1>');
    //     mywindow.document.write(document.getElementById(elem).innerHTML);
    //     mywindow.document.write('</body></html>');

    //     mywindow.document.close(); // necessary for IE >= 10
    //     mywindow.focus(); // necessary for IE >= 10*/

    //     mywindow.print();
    //     mywindow.close();

    //     return true;
    // }

    const onClickSaveAndExport = () => {
        html2canvas(document.querySelector("#order-invoice")).then(async (canvas) => {
            const imgData = canvas.toDataURL('image/png');
            const pdf = new jsPDF({
                orientation: 'landscape',
            });

            pdf.addImage(imgData, 'PNG', 0, 0);
            await pdf.save(`${orderPayment.title}.pdf`);

            notification.success({
                message: 'Lưu',
                description: 'Lưu đơn hàng và xuất hóa đơn thành công'
            })

            setTimeout(() => {
                router.push('/NewOrder')
            }, 1000)
        });
    }

    return (
        <Row className='invoice'>
            <Col id={'order-invoice'} span={19} style={{ padding: '10px 20px', overflow: 'auto', height: '100vh' }} className='invoice__right-content br--1-grey '>
                <div className='d-flex-center column h-max-content'>
                    <div className='d-flex a-center'>
                        <img src='/images/logo.png' width={40} />
                        <Title style={{ margin: '0px 0px 0px 10px' }} level={3}>Vuifun shop - Cửa hàng thời trang</Title>
                    </div>
                    <Text><Text strong>Địa chỉ:</Text> 47-48 Lê Thị Riêng Q.1</Text>
                    <Text><Text strong>Điện thoại:</Text> 0989289182</Text>
                    <Title level={2} style={{ margin: '10px 0px 0px 0px' }}>HÓA ĐƠN BÁN HÀNG</Title>
                    <Text>Số HĐ: HD00001</Text>
                    <Text>{infoOrder.time}</Text>
                </div>
                <Space direction="vertical">
                    <Text><Text strong>Khách hàng:</Text> {customer.name}</Text>
                    <Text><Text strong>Số điện thoại:</Text> {customer.phone}</Text>
                </Space>
                <Table bordered size='small' pagination={false} columns={columns} dataSource={newProducts} style={{ marginTop: 10 }} />
                <div className='d-flex column a-end mt-10'>
                    <div className='d-flex j-space-between mw-300'>
                        <Text strong>Tổng tiền hàng</Text>
                        <Text>{numeral(infoOrder.total).format('0,0')} vnđ</Text>
                    </div>
                    <div className='d-flex j-space-between mw-300'>
                        <Text strong>Giảm giá</Text>
                        <Text>{numeral(infoOrder.discount).format('0,0')} vnđ</Text>
                    </div>
                    <div className='d-flex j-space-between mw-300'>
                        <Text strong>Tổng thanh toán</Text>
                        <Text>{numeral(infoOrder.customer_pay).format('0,0')} vnđ</Text>
                    </div>
                </div>
                <div className='d-flex j-center mt-10'><i>Xin cảm ơn và hẹn gặp lại!</i></div>
            </Col>
            <Col span={5} className='invoice__left-content' style={{ height: '100vh', textAlign: 'center' }}>
                <Title level={3}>Thông tin hóa đơn</Title>
                <div className='d-flex column'>
                    <Button onClick={onClickSave} type='primary' size='large' style={{ width: '100%', marginBottom: 10 }}>
                        <strong>Lưu</strong>
                    </Button>
                    <Button onClick={onClickSaveAndExport} type='primary' size='large' style={{ width: '100%', background: '#389e0d', border: '1px solid #389e0d' }}>
                        <strong>Lưu và xuất hóa đơn</strong>
                    </Button>
                </div>
            </Col>
        </Row>
    );
};

InVoice.propTypes = {};

export default InVoice;