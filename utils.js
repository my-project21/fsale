export const random = (number) => {
    try {
        let text = '';
        let possible = 'abcdefghijklmnopqrstuvwxyz0123456789';

        for (let i = 0; i < number; i++) { text += possible.charAt(Math.floor(Math.random() * possible.length)) }

        return text;
    } catch (error) {
        handleError(error, {
            path: PATH,
            name: 'random',
            args: {
                number
            }
        });
    }
};